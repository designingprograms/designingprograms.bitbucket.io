# Designing Programs

{ a pedagogical course on computational design for visual arts students }

[![PyPI](https://img.shields.io/pypi/l/fsfe-reuse.svg)](https://www.gnu.org/licenses/gpl-3.0.html)
[![reuse compliant](https://img.shields.io/badge/reuse-compliant-green.svg)](https://git.fsfe.org/reuse/reuse)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
---
![designingPrograms_esadA_2018.jpg](https://bitbucket.org/repo/z8Bkzgo/images/3470683451-designingPrograms_esadA_2018.jpg)

## Introduction

Designing Programs is a practical and pedagogical approach to programming, tailored for visual arts students. This course is both a means for learning the fundamentals of programming as well as pertaining to be a source for exploring the medium of code in artistic practices.

This repository is a collection of concrete examples that demonstrate the common principles of programming. It is written for Processing users.

## Contents
I'll be adding source files as the project progresses.

* [DP_Processing - Processing sketches](https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/examples_source_code/dp_processing/)
* [DP_P5js - JavaScript(P5js) sketches](https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/examples_source_code/dp_p5js/)
* ...
* Tools - Some helpful tools

## Website

The course is accompanied by a website : [Designing Programs](https://designingprograms.bitbucket.io)
This website will also (very soon) be accompanied by an on-going collection of sketches that introduce some important graphic design principles. My aim is to demonstrate the use of these priniciples in computational media.

## Install

The programs in this repository can be compiled using the Processing environment or with P5js in a browser.

[https://processing.org/](https://processing.org/)
[https://p5js.org/](https://p5js.org/)

## Contact & Sundries

* mark.webster@wanadoo.fr
* Version v0.05
* Tools used : Processing & P5js

## Contribute
To contribute to this project, please fork the repository and make your contribution to the
fork, then open a pull request to initiate a discussion around the contribution.

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

For more information https://www.gnu.org/licenses/gpl-3.0.en.html

The program in this repository meet the requirements to be REUSE compliant,
meaning its license and copyright is expressed in such as way so that it
can be read by both humans and computers alike.

For more information, see https://reuse.software/

/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: loops_02
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/
var margin;
var interval;

/////////////////////////// SETUP ////////////////////////////
function setup() {
  var canvas = createCanvas(680, 360);
  canvas.parent('canvas');
  background(255);
  smooth();
  //noStroke();
  noFill();
  strokeWeight(2);
  rectMode(CENTER);
  margin = 50;
  interval = 50;

}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0);
  stroke(255);
  for (var y=margin; y<height-margin; y+=interval) {
    for (var x=margin; x<width-margin; x+=interval) {
      var dia = cos(frameCount*0.015) * 135;
      ellipse(x, y, dia, dia);
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////

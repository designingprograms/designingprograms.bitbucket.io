/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: function_01
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/

function setup() {
  var c = createCanvas(680, 360);
  c.parent('canvas');
  background(0, 0, 33);
}


function draw() {

  let crossLen = map(slider1.value/100, 0, 1, 1, 100);
  let angle = map(slider2.value/100, 0, 1, 0.0,360.0);

  background(0, 0, 33);
    fill(0, 0, 255);
    cross(175, 180, crossLen+250, angle);

    fill(150, 255, 0);
    cross(425, 180, crossLen+100, angle);

    fill(255, 200, 0);
    cross(550, 180, crossLen, angle);
}

// Here is our function definition
// - It has a name.
// - Arguments (although not necessary).
// - And the definition between curly brackets.

function cross(_cntX, _cntY, _s, a) {
  rectMode(CENTER);
  noStroke();
  push();
  translate(_cntX, _cntY);
  rotate(radians(a));
  rect(0, 0, _s, _s/4);
  rect(0, 0, _s/4, _s);
  pop();
}


// Slider update:
function outputUpdateSlider1(v) {
  document.querySelector('#size').value = v;
}
function outputUpdateSlider2(v) {
  document.querySelector('#angle').value = v;
}

/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: control_02
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/

var myColour;
var rectSize;

function setup() {
  var canvas = createCanvas(680, 360);
  canvas.parent('canvas');
  background(33);
  smooth();
  rectMode(CENTER);
  myColour = color(255);
  rectSize = 250;
}

function draw() {
  background(33);
  noStroke();
  fill(myColour);
  rect(width/2, height/2, rectSize, rectSize);

  infos(); // display infos ;–)
}

/////////////////////////// FUNCTIONS ////////////////////////////
// REF : https://p5js.org/reference/#/p5/keyTyped
function keyTyped(){
  if (key == 'r') {
      // Note the 3 local varialbes here
      // each storing a random value that is generated on each press of the key, 'r'

      var rouge = random(255); // random value between 0 et 255
      var vert = random(255);
      var bleu = random(255);
      // we then assign these random values to our colour variable
      myColour = color(rouge, vert, bleu);

      rectSize = random(100,320); // random value for size of shape
    }
}

// function for displaying textual info
function infos(){
 fill(255);
 textSize(18);
 text("Press r", 10, height-45);
 textSize(12);
 text("Chosen colour : " + myColour, 10, height-28);
 text("Chosen size : " + rectSize, 10, height-10);
}

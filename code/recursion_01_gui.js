/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: recursion_01
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/

function setup() {
  var c = createCanvas(680, 360);
  c.parent('canvas');
  background(0, 0, 33);
  smooth();
  noFill();
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0, 0, 33);
  stroke(255);
  strokeWeight(0.5);

  let rad = slider1.value;
  let fact = slider2.value;

  drawCircle(width/2, height/2, rad, fact);
}


/**
 * Our recursive function
 */
 function drawCircle(x, y, radius, fact) {
   ellipse(x, y, radius/2, radius/2);
   if (radius > 12.0) {
     radius *= fact;
     drawCircle(x+radius/1.75, y, radius/2, fact);
     drawCircle(x-radius/1.75, y, radius/2, fact);
     drawCircle(x, y+radius/2, radius/2, fact);
     drawCircle(x, y-radius/2, radius/2, fact);

   }
 }

// Slider update:
function outputUpdateSlider1(v) {
  document.querySelector('#rad').value = v;
}
function outputUpdateSlider2(v) {
  document.querySelector('#fact').value = v;
}

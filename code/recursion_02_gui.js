/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: recursion_02
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/

function setup() {
  var c = createCanvas(680, 360);
  c.parent('canvas');
  background(0, 0, 33);
  smooth();
  noFill();
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0, 0, 33);
  stroke(255);

  let numIter = slider1.value;
  tree(numIter, 340, height-65, -HALF_PI, 75, 3);

}


/*
 * @param :
 * n = number of iterations
 * x, y = coordinate positions
 * a = angle for tree
 * branchRadius = length of each branch
 * _w = initial strokweight for line
 */
function tree(n, x, y, a, branchRadius, _w) {
  //let bendAngle   = radians(random(-8,17));
  //let branchAngle = radians(random(15,45));
  let bendAngle = radians(slider2.value);
  let branchAngle = radians(slider3.value);

  const branchRatio = 0.76;
  //float branchRatio = map(branchRadius, 1, 80, .785, .799);
  let weight= _w;
  _w*=branchRatio;

  let cx = x + cos(a) * branchRadius;
  let cy = y + sin(a) * branchRadius;

  strokeWeight(weight);
  line(x, y, cx, cy);
  if (n == 0) return;
  tree(n-1, cx, cy, a + bendAngle - branchAngle, branchRadius * branchRatio, _w);
  tree(n-1, cx, cy, a + bendAngle + branchAngle, branchRadius * branchRatio, _w);
}


// Slider update:
function outputUpdateSlider1(v) {
  document.querySelector('#num').value = v;
}
function outputUpdateSlider2(v) {
  document.querySelector('#bendA').value = v;
}

function outputUpdateSlider3(v) {
  document.querySelector('#branchA').value = v;
}

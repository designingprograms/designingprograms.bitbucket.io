/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: loops_01
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/

/////////////////////////// SETUP ////////////////////////////
function setup() {
 createCanvas(680, 360);
 background(33);
 smooth();
 noStroke();
 rectMode(CENTER);
 // The loop structure has :
 // - A variable with an intitial value >>> int i=0;
 // - A condition >>> i<5000;
 // - A step value/increment >>> i++ (i = i + 1)

 // Then...
 // All instructions within the main body of the loop
 // will be executed as long as we reamain within the loop
 // i.e. that the condition is still false
 for (var i=0; i<5000; i++) {

   var x = random(width); // a random value for x on each iteration
   var y = random(height); // a random value for y on each iteration
   var dia = random(1, 9); // a random value for dia on each iteration

   // we draw our shape
   ellipse(x, y, dia, dia);
 }
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
 //background(33);

}

/////////////////////////// FUNCTIONS ////////////////////////////
// we use keyTyped in JavaScript P5
// REF : https://p5js.org/reference/#/p5/keyTyped
function keyTyped(){
     setup();
}

/**
* Designing Programs
* https://designingprograms.bitbucket.io
*
* Sketch: loops_02
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/

let margin;
let interval;
let size;

/////////////////////////// SETUP ////////////////////////////
function setup() {
 createCanvas(680, 360);
 background(33);
 smooth();
 noStroke();
 margin = 50;
 interval = 15;
 size = 10;

 // The loop structure has :
 // - A variable with an intitial value >>> int i=0;
 // - A condition >>> i<width;
 // - A step value/increment >>> i+=interval (i = i + interval)

  for (var y=margin; y<height-margin; y+=interval) {
    for (var x=margin; x<width-margin; x+=interval) {

      // we draw our shape
      ellipse(x, y, size, size);
    }
  }
 }
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
 //background(33);
}
/////////////////////////// FUNCTIONS ////////////////////////////

/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: class_01
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/
let myBall;

function setup() {
  var canvas = createCanvas(680, 360);
  canvas.parent('canvas');
  background(33);
  smooth();
  noStroke();
  rectMode(CENTER);
  myBall = new Ball();
  //print(myBall.x);

}

function draw() {
  background(33);

  //methods
  myBall.display();
  myBall.move();
  myBall.check();


}

/////////////////////////// CLASS ////////////////////////////

/**
 * Here is the structure of our class
 *
 */

class Ball {
  // The constructor enables the creation of objects.
  // ***!IMPORTANT!***
  constructor(){
    // Our class fields (global variables that belong to our class)
    this.x = width/2;
    this.y = height/2;
    this.dia = random(50, 100);
    this.vx = 5;
    this.vy = 3.7;
  }

  // Next we define our methods
   display() {
    fill(255);
    //note the use of our fields (variables)
    ellipse(this.x, this.y, this.dia, this.dia);
  }

  // A method to move the ball
    move() {
    this.x+=this.vx;
    this.y+=this.vy;
  }


  // A method to test the ball's position
    check() {
    if (this.x<0 + this.dia/2) {
      //vx = vx * -1 (THIS MAKES ANY VALUE NEGATIVE).
      this.vx*=-1;
    }
    if (this.x>width - this.dia/2) {
      this.vx*=-1;
    }
    if (this.y<0 + this.dia/2) {
      this.vy*=-1;
    }
    if (this.y>height - this.dia/2) {
      this.vy*=-1;
    }
  }
}

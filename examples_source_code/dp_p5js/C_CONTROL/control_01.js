/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: control_01
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/

let myColour;

function setup() {
  var canvas = createCanvas(680, 360);
  canvas.parent('canvas');
  background(33);
  smooth();
  rectMode(CENTER);
  myColour = color(255);
}

function draw() {
  background(33);

  fill(myColour);
  rect(width/2, height/2, 250, 250);

  infos(); // display textual information
}

/////////////////////////// FUNCTIONS ////////////////////////////
// we use keyTyped function to add interaction with the keyboard
// REF : https://p5js.org/reference/#/p5/keyTyped
function keyTyped(){
  if (key == 'a') {
     myColour = color(0, 0, 255);
   }

   if (key == 'b') {
     myColour = color(200, 255, 0);
   }

    if (key == 'c') {
     myColour = color(255, 0, 255);
   }
}

// a function that displays textual info
function infos(){
 fill(255);
 textSize(18);
 text("Press a | b | c", 10, height-30);
 textSize(12);
 text("Chosen colour : " + myColour, 10, height-12);
}

/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: function_01
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
 */
function setup() {
  var c = createCanvas(680, 360);
  c.parent('canvas');
  background(0, 0, 33);
}


function draw() {

  background(0, 0, 33);
    fill(0, 0, 255);
    cross(150, 155, 250);

    fill(150, 255, 0);
    cross(400, 155, 200);

    fill(255, 200, 0);
    cross(575, 155, 100);
}

// Here is our function definition
// - It has a name.
// - Arguments (although not necessary).
// - And the definition between curly brackets.

function cross(_cntX, _cntY, _size) {
  rectMode(CENTER);
  noStroke();
  push();
  translate(_cntX, _cntY);
  rect(0, 0, _size, _size/4);
  rect(0, 0, _size/4, _size);
  pop();
}

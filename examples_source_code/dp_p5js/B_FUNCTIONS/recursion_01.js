/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: function_03
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
 */
function setup() {
  var c = createCanvas(680, 360);
  c.parent('canvas');
  background(0, 0, 33);
  smooth();
  noFill();
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0, 0, 33);
  stroke(255);
  strokeWeight(1.5);

  var rad = map(sin(frameCount*0.015), -1, 1, 50, 600);
  drawCircle(width/2, height/2, rad, 0.87);
}


/**
 * Our recursive function
 */
 function drawCircle(_x, _y, _radius, _fact) {
   ellipse(_x, _y, _radius/2, _radius/2);
   if (_radius > 12.0) {
     _radius *= _fact;
     drawCircle(_x+_radius/1.75, _y, _radius/2, _fact);
     drawCircle(_x-_radius/1.75, _y, _radius/2, _fact);
     drawCircle(_x, _y+_radius/2, _radius/2, _fact);
     drawCircle(_x, _y-_radius/2, _radius/2, _fact);

   }
 }

/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: function_02
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
 */
function setup() {
  var c = createCanvas(680, 360);
  c.parent('canvas');
  background(0, 0, 33);
  smooth();
  noStroke();

}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0, 0, 33);
  // We declare a variable and assign it our function below.
  // This function returns a value that varies according to
  // an oscillation calculation
  var crossSize = oscillate();

  // we then pass this variable as an argument to our cross function
  // modifying the size of our cross
  fill(233,255,0);
  cross(width/2, height/2, crossSize);


}

/**
 * This is a new function that returns a value
 * Again, we give it a name and define it's use within the curly brackets.
 * Notice the keyword 'return' which is used to send our value
 */
function oscillate() {
  var val = sin(frameCount*0.015)*250;
  return val;
}

function cross(_cntX, _cntY, _size) {
  rectMode(CENTER);
  noStroke();
  push();
  translate(_cntX, _cntY);
  rect(0, 0, _size, _size/4);
  rect(0, 0, _size/4, _size);
  pop();
}

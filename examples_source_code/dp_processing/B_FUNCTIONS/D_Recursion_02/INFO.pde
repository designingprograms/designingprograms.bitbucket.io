/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: Recursion_02
 * Parent Sketch: Recursion_01
 * Type: dynamic
 *
 * Summary : Demonstrates recursion & the L-System.
 *
 *
 * GIT:
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */

/*
 * Recursion Trees_02
 * Tree function by Robert Sedgewick & Kevin Wayne
 * link : http://introcs.cs.princeton.edu/java/23recursion/Tree.java.html
 * A more elegant version of the tree algorithm in my opinion
 *
 * REF : http://introcs.cs.princeton.edu/java/23recursion/
 *
 */

////////////////////////// GLOBALS ///////////////////////////
int SEED;
int numIterations;
////////////////////////// SETUP ///////////////////////////
void setup() {
  size(680, 420);
  smooth();
  background(0,0,33);
  SEED = (int)random(1000);
}

////////////////////////// DRAW ///////////////////////////

void draw() {
  randomSeed(SEED);
  background(0,0,33);
  pushMatrix();
  translate(width/2, height);
  stroke(217);
  numIterations = (int)map(mouseX, 0, width, 1, 18);

  tree(numIterations, 0, -65, -HALF_PI, 90, 6);

  popMatrix();
  displayInfo();
}



////////////////////////// FUNCTIONS ////////////////////
void mousePressed() {
  setup();
}


/*
 * @param :
 * n = number of iterations
 * x, y = coordinate positions
 * a = angle for tree
 * branchRadius = length of each branch
 * _w = initial strokweight for line
 */
void tree(int n, float x, float y, float a, float branchRadius, float _w) {
  float bendAngle   = radians(random(-8,17));
  float branchAngle = radians(random(15,45));

  float branchRatio = 0.76;
  //float branchRatio = map(branchRadius, 1, 80, .785, .799);
  float weight= _w;
  _w*=branchRatio;

  float cx = x + cos(a) * branchRadius;
  float cy = y + sin(a) * branchRadius;

  stroke(217);
  strokeWeight(weight);

  line(x, y, cx, cy);

  if (n == 0) return;

  tree(n-1, cx, cy, a + bendAngle - branchAngle, branchRadius * branchRatio, _w);
  tree(n-1, cx, cy, a + bendAngle + branchAngle, branchRadius * branchRatio, _w);
}

void displayInfo(){
  float x=20;
  float y=height-80;
  float spacing=20;

  text("PARAMETERS",x, y);
  text("iterations : "+numIterations,x, y+spacing);
  text("click to re-generate", x, y+spacing*2);

}
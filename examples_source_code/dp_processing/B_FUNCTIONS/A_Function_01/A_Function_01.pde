/*
----------------------------------------
 PLEASE READ INFO TAB FOR ALL INFORMATION
 ----------------------------------------
 */
/////////////////////////// GLOBALS ////////////////////////////

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(680, 360);
  background(0, 0, 33);
  smooth();
  noStroke();

}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0, 0, 33);
  fill(0, 0, 255);
  cross(150, 155, 250);

  fill(150, 255, 0);
  cross(400, 155, 200);

  fill(255, 200, 0);
  cross(575, 155, 100);
}

/////////////////////////// FUNCTIONS ////////////////////////////
// Here is our function definition
// - It has a name.
// - Arguments (although not necessary).
// - And the definition between curly brackets.

void cross(float _cntX, float _cntY, float _size) {
  rectMode(CENTER);
  pushMatrix();
  translate(_cntX, _cntY);
  rect(0, 0, _size, _size/4);
  rect(0, 0, _size/4, _size);
  popMatrix();
}
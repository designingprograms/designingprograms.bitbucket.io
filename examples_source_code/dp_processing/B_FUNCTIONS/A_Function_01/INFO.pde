/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: Function_01
 * Parent Sketch: none
 * Type: static
 *
 * Summary : Demonstrates how to create a function for drawing something.
 *
 * GIT: 
 * Author: mark webster 2018
 * https://designingprograms.bitbucket.io/
 */
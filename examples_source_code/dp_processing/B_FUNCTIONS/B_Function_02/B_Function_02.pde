/*
----------------------------------------
 PLEASE READ INFO TAB FOR ALL INFORMATION
 ----------------------------------------
 */
/////////////////////////// GLOBALS ////////////////////////////

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(680, 360);
  background(0, 0, 33);
  smooth();
  noStroke();

}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0, 0, 33);
  // We declare a variable and assign it our function below.
  // This function returns a value that varies according to
  // an oscillation calculation
  float crossSize = oscillate();

  // we then pass this variable as an argument to our cross function
  // modifying the size of our cross
  fill(233,255,0);
  cross(width/2, height/2, crossSize);


}

/**
 * This is a new function that returns a value
 * Again, we give it a name and define it's use within the curly brackets.
 * Notice the keyword 'return' which is used to send our value
 */
float oscillate() {
  float val = sin(frameCount*0.015)*250;
  return val;
}

void cross(float _cntX, float _cntY, float _size) {
  rectMode(CENTER);
  noStroke();
  pushMatrix();
  translate(_cntX, _cntY);
  rect(0, 0, _size, _size/4);
  rect(0, 0, _size/4, _size);
  popMatrix();
}
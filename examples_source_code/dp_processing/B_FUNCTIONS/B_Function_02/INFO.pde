/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: variables_01
 * Parent Sketch: none
 * Type: autonomous
 *
 * Summary : Demonstrates how to create a function that returns a value.
 *
 * GIT:
 * Author: mark webster 2018
 * https://designingprograms.bitbucket.io/
 */
/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: Recursion_01
 * Parent Sketch: None
 * Type: dynamic
 *
 * Summary : Demonstrates recursion.
 *
 * GIT:
 * Author: mark webster 2018
 * https://designingprograms.bitbucket.io/
 */
/*
----------------------------------------
 PLEASE READ INFO TAB FOR ALL INFORMATION
 ----------------------------------------
 */
/////////////////////////// GLOBALS ////////////////////////////

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(680, 460);
  background(0, 0, 33);
  smooth();
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0, 0, 33);
  stroke(255);
   noFill();
   float rad = map(mouseX, 0, width, 1, 700);
   float f = map(mouseY, 0, height, 0.735, 0.999999);

   pushMatrix();
   translate(width/2, height/2);
   drawCircle(0, 0, rad, f);
   popMatrix();


}

/**
 * Our recursive function
 */
 void drawCircle(float x, float y, float radius, float _fact) {
   strokeWeight(0.5);
   ellipse(x, y, radius/2, radius/2);
   if (radius > 12.0) {
     radius *= _fact;
     drawCircle(x+radius/2, y, radius/2, _fact);
     drawCircle(x-radius/2, y, radius/2, _fact);
     drawCircle(x, y+radius/2, radius/2, _fact);
     drawCircle(x, y-radius/2, radius/2, _fact);
   }
 }
/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


/////////////////////////// GLOBALS ////////////////////////////
Ball[] myBalls; // declare a list of objects

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(860, 540);
  background(0,0,33);
  smooth();
  noStroke();
  
  // create our objects
  myBalls = new Ball[100]; // number of objects = 100
  
  // We use a for loop to create a unique instance/object for each 100
  for(int i=0; i<myBalls.length; i++){
    myBalls[i] = new Ball(); 
  }


}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0,0,33);
  
  for(int i=0; i<myBalls.length; i++){
    myBalls[i].display();
    myBalls[i].move();
    myBalls[i].check();
  }
  
  //display infos
  infos();
}

/////////////////////////// FUNCTIONS ////////////////////////////

void keyPressed(){
 setup(); 
}

// simple function for displaying info
void infos(){
 fill(255);
 textSize(18);
 text("Press r", 10, height-35);
}
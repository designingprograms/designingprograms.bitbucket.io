/**
 * Here is the class structure
 *
 */


class Ball {
  // declare our data fields
  float x;
  float y;
  float dia;
  float vx, vy;
  color col;

  // The constructor enables us to create an instance of
  // our class - an object
  Ball() {
    x = width/2;
    y = height/2;
    dia = random(5, 50); 
    vx = random(-5,5);
    vy = random(-3.3, 3.3);
    col = color(random(255),random(255),random(255));
  }

   // here are our method definitions. 
  void display() {
    fill(col);
    //Note that we use our data fields.
    ellipse(x, y, dia, dia);
  }

  // Method for moving our ball   
  void move() {
    x+=vx;
    y+=vy;
  }


  // Method to keep our ball within the limits of the screen
  void check() {
    if (x<0 + dia/2) {
      vx*=-1;
    }
    if (x>width - dia/2) {
      vx*=-1;
    }
    if (y<0 + dia/2) {
      vy*=-1;
    }
    if (y>height - dia/2) {
      vy*=-1;
    }
  }
}
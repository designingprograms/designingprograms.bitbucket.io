/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


/////////////////////////// GLOBALS ////////////////////////////
Ball myBall; // declare an object

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(860, 540);
  background(0,0,33);
  smooth();
  noStroke();
  myBall = new Ball(); // create our object

}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0,0,33);
  
  //call our methods on our object 
  myBall.display();
  myBall.move();
  myBall.check();
  
  //display infos
  infos();
}

/////////////////////////// FUNCTIONS ////////////////////////////

void keyPressed(){
 setup(); 
}

// simple function for displaying info
void infos(){
 fill(255);
 textSize(18);
 text("Press r", 10, height-45);
 textSize(12);
 text("Size = " + myBall.dia, 10, height-25);
}
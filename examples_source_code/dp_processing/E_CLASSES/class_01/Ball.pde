/**
 * Here is the class structure
 *
 */


class Ball {
  // declare our data fields
  float x;
  float y;
  float dia;
  float vx, vy;

  // The constructor enables us to create an instance of
  // our class - an object
  Ball() {
    x = width/2;
    y = height/2;
    dia = random(50, 100); 
    vx = 5;
    vy = 3.7;
  }

  // here are our method definitions. 
  void display() {
    fill(255);
    //Note that we use our data fields.
    ellipse(x, y, dia, dia);
  }

  // Method for moving our ball   
  void move() {
    x+=vx;
    y+=vy;
  }


  // Method to keep our ball within the limits of the screen
  void check() {
    if (x<0 + dia/2) {
      vx*=-1;
    }
    if (x>width - dia/2) {
      vx*=-1;
    }
    if (y<0 + dia/2) {
      vy*=-1;
    }
    if (y>height - dia/2) {
      vy*=-1;
    }
  }
}
/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: Class_01
 * Parent Sketch: none
 * Type: autonomous
 *
 * Summary : Demonstrates classes and objects
 *
 * GIT: https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/examples_source_code/dp_processing/
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */
 
/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: Control_02
 * Parent Sketch: none
 * Type: static
 *
 * Summary : Demonstrates the random function.
 *
 * GIT: https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/examples_source_code/dp_processing/
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */
 
/////////////////////////// GLOBALS ////////////////////////////
color myColour = color(255);
float dia = 250;

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(600, 420);
  background(0,0,33);
  smooth();
  noStroke();
  rectMode(CENTER);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0,0,33);

  fill(myColour);
  rect(width/2, height/2, dia, dia);
  
  infos(); // infos ;–)
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key == 'r') {
    // Note we are creaeting 3 local variables
    // each is assigned a random value between 0 > 255
    
    float rouge = random(255);  
    float vert = random(255);  
    float bleu = random(255);  
    
    // we then apply these variables to our color function
    myColour = color(rouge, vert, bleu);
    
    dia = random(100,320); // a random number for the size of our ellipse
  }
}

// Function for displaying text
void infos(){
 fill(255);
 textSize(18);
 text("Press 'r'", 10, height-45);
 textSize(12);
 text("Chosen colour : " + hex(myColour), 10, height-25);
 text("Chosen size : " + dia, 10, height-10);
}
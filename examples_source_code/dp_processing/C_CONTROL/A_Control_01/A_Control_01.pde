/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: Control_01
 * Parent Sketch: none
 * Type: static
 *
 * Summary : Demonstrates the if statement.
 *
 * GIT: https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/examples_source_code/dp_processing/
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */
 
/////////////////////////// GLOBALS ////////////////////////////
color myColour = color(255);

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(600, 420);
  background(0,0,33);
  smooth();
  noStroke();
  rectMode(CENTER);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0,0,33);

  fill(myColour);
  rect(width/2, height/2, 250, 250);
  
  infos();
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key == 'a') {
    myColour = color(0, 0, 255);
  }

  if (key == 'b') {
    myColour = color(200, 255, 0);
  }
  
   if (key == 'c') {
    myColour = color(0, 200, 255);
  }
}

// Function for displaying text
void infos(){
 fill(255);
 textSize(18);
 text("Press keys a | b | c", 10, height-30);
 textSize(12);
 text("Chosen colour : " + hex(myColour), 10, height-15);
}
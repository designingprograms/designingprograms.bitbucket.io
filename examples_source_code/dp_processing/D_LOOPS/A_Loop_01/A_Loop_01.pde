/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: Loop_01
 * Parent Sketch: none
 * Type: static
 *
 * Summary : Demonstrates iteration with the FOR loop.
 *
 * GIT: https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/examples_source_code/dp_processing/
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */
 
/////////////////////////// GLOBALS ////////////////////////////

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(1200, 600);
  background(33);
  smooth();
  noStroke();

  // The for structure is comprised of :
  // - a variable with an initial value > int i=0;
  // - a condition > if the variable value is less than ...
  // - a step/increment value
  
  // ...
  // As long as the condition is true, the for loop repeats all
  // instructions within the body - between the two curly brackets
  for (int i=0; i<5000; i++) {
    
    float x = random(width); // random value for x on each repetition
    float y = random(height); // random value for y on each repetition
    float dia = random(1, 9); // random value for dia on each repetition
    
    // we draw 4999 ellipses at different positions and with different sizes.
    ellipse(x, y, dia, dia);
  }
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  //background(33);
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed(){
 setup(); 
 
 //save an image 
 if(key == 's'){
   saveFrame("Export_###.png");
 }
}
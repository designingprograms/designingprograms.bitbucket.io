/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: Loop_02
 * Parent Sketch: Loop_01
 * Type: static
 *
 * Summary : Demonstrates how to draw a grid of forms
 *
 * GIT: https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/examples_source_code/dp_processing/
 * Author: mark webster 2019
 * https://designingprograms.bitbucket.io/
 */
 
/////////////////////////// GLOBALS ////////////////////////////
int margin = 25;
float dia = 10;
int interval = 20;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(600, 460);
  background(33);
  smooth();
  noStroke();
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(33);

  for (int y=margin; y<height-margin; y+=interval) {
    for (int x=margin; x<width-margin; x+=interval) {

      ellipse(x, y, dia, dia);
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
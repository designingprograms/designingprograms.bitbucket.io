/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: variables_02
 * Parent Sketch: variables_01
 * Type: dynamic
 *
 * Summary : Demonstrates different data types
 *
 * GIT: 
 * Author: mark webster 2018
 * https://designingprograms.bitbucket.io/
 */
/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////

// We are declaring various global variables :
// 1). data type (int, float, String...)
// 2). variable name (x, y, myCouleur...)
// 3). & eventually an initial value

int x = 50;                         // whole integer number
float y = 253.078;                  // decimal number

String theWord = "WORD";                // text
String infos = "Press 'b'";           // text
char theLetter = 'A';                  // a letter
boolean isDisplay = false;           // boolean > YES/NO | ON/OFF ...

color myc; // Note we have given no initial value here

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(680, 360);
  background(33);
  smooth();
  noStroke();
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(33);
  
  textSize(200);
  if (isDisplay) {
    // we are giving an initial colour here
    myc = color(0, 0, 255);
    fill(myc);
    text(theWord, x, y);
  } else 
  {
    // & changing the colour depending on our boolean value
    // when display is true, its blue, otherwise its yellow
    myc = color(255, 200, 0);
    fill(myc);
    text(theLetter, x, y);
  }
  
  textSize(24);
  fill(255);
  text(infos, x, 310);
}

/////////////////////////// FUNCTIONS ////////////////////////////
// This is an event function. When we press a key (the key 'b' here)
// we can execute something - in this case we change the value of our 
// boolean to true or false
void keyPressed() {
  if (key == 'b') {
    isDisplay = !isDisplay;
  }
  // this is a really handy way to debug or just display information in the console
  println(isDisplay);
}
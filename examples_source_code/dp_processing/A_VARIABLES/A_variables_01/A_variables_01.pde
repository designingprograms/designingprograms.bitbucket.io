/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////
// We declare 2 variables and initialise with 2 starting values
float x = 0;
float dia = 0;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(680, 360);
  background(33);
  smooth();
  noStroke();
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(33);
  
  // Note the use of our variables in the function
  ellipse(x, height/2, dia, dia);
  
  // We update our variables
  x = x + 2;
  dia = dia + 0.5;
  
}

/////////////////////////// FUNCTIONS ////////////////////////////
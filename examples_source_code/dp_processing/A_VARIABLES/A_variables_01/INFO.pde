/*
 * ::::::::::::::::::
 * DESIGNING PROGRAMS
 * ::::::::::::::::::
 *
 * Sketch: variables_01
 * Parent Sketch: none
 * Type: autonomous
 *
 * Summary : Demonstrates a variable for storing data
 *
 * GIT:
 * Author: mark webster 2018
 * https://designingprograms.bitbucket.io/
 */
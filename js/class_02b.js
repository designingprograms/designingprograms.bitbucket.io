/**
 * Designing Programs
 * Web site: https://designingprograms.bitbucket.io
 *
 * Sketch: loops_01
 * version: V0.05
 * Author: m.Webster 2019
 * https://area03.bitbucket.io/
 *
 */
/////////////////////////// SETUP ////////////////////////////

let sketch1 = function(sketch) {

    let ball_01;
    let ball_02;
    let isChange = false;

    sketch.setup = function() {
      sketch.createCanvas(680, 360);
      sketch.background(0, 0, 33);
      sketch.smooth();
      sketch.noStroke();
      sketch.ball_01 = new Ball(200, sketch.height/2);
      sketch.ball_02 = new Ball(450, sketch.height/2);
    };

    sketch.draw = function() {
      //sketch.background(0, 0, 33);
      sketch.fill(0,0,33,13);
      sketch.rect(0, 0, sketch.width, sketch.height);


      //our methods
      sketch.ball_01.oscillateY(0.06, 135);
      if(isChange){
          sketch.ball_01.oscillateDia(0.035, 90);
      }else {
          sketch.ball_01.oscillateX(0.095, 60);
      }
      sketch.ball_01.display();


      if(isChange){
        sketch.ball_02.oscillateY(0.06, 70);
        sketch.ball_02.oscillateDia(0.05, 75);
      }else {
        sketch.ball_02.oscillateDia(0.035, 135);
      }
      sketch.ball_02.display();

      sketch.fill(255);
      sketch.textSize(14);
      //sketch.text("Press ' r '", 10, 300);
      sketch.text("Press ' m '", 10, 350);


    };

    sketch.keyTyped = function() {
      if (sketch.key === 'r') {
        sketch.setup();
      }
      if (sketch.key === 'm') {
        isChange = !isChange;
      }
    }

      class Ball {

        // The constructor enables the creation of objects.
        // ***!IMPORTANT!***
        constructor(_x, _y) {
          // Our class fields (global variables that belong to our class)
          this.x = _x;
          this.y = _y;
          this.dia = sketch.random(25, 75);
          this.offDia = 0;
          this.vx = 0;
          this.vy = 0;
        }

        // Next we define our methods
        display() {
          sketch.fill(255);
          //note the use of our fields (variables)
          sketch.ellipse(this.x + this.vx, this.y + this.vy, this.dia + this.offDia, this.dia + this.offDia);
        }


        // our various methods
        oscillateX(_freq, _amp) {
          this.vx = sketch.sin(sketch.frameCount * _freq) * _amp;
        }

        oscillateY(_freq, _amp) {
          this.vy = sketch.sin(sketch.frameCount * _freq) * _amp;
        }

        oscillateDia(_freq, _amp) {
            this.offDia = sketch.sin(sketch.frameCount * _freq) * _amp;
        }

      };
    };

    let myp5 = new p5(sketch1, 'sketch1');


    /////////////////////////////////////// Sketch 2 >>>
    let sketch2 = function(sketch) {
      theBalls = []; // an array of objects
      let numberOfBalls;
      let isDrawing = false;

      sketch.setup = function() {
        sketch.createCanvas(680, 360);
        sketch.background(0, 0, 33);
        sketch.smooth();
        sketch.rectMode(sketch.CENTER);
        numberOfBalls = 200;
        // to instantiate our array of objects, we use a FOR loop
        for (var i = 0; i < numberOfBalls; i++) {
          theBalls[i] = new Ball();
        }
      }

      sketch.draw = function() {
        if(isDrawing){

        }else {
          sketch.background(0, 0, 33);
        }

        // to read our array of objects and call methods on them, we also use a FOR loop
        for (var i = 0; i < numberOfBalls; i++) {
          theBalls[i].display();
          theBalls[i].move();
          theBalls[i].check();
          if(isDrawing){
            theBalls[i].dia = 3;
            theBalls[i].oscillateDia(0.025, 5);
            theBalls[i].randomize();
          }
        }

        sketch.fill(255);
        sketch.textSize(14);
        sketch.text("Press ' r '", 10, 335);
        sketch.text("Press ' p '", 10, 350);

      }


      sketch.keyTyped = function() {
        if (sketch.key == 'r') {
          sketch.setup();
        }
        if (sketch.key == 'p') {
          isDrawing =!isDrawing;
        }

      }


      /////////////////////////// CLASS ///////////////////////
      class Ball {

        // The constructor enables the creation of objects.
        // ***!IMPORTANT!***
        constructor() {
          // Our class fields (global variables that belong to our class)
          this.x = sketch.width / 2;
          this.y = sketch.height / 2;
          this.dia = sketch.random(5, 35);
          this.offDia = 0;
          this.vx = sketch.random(-3, 3);
          this.vy = sketch.random(-3, 3);
          this.myCol = sketch.color(sketch.random(33, 220), sketch.random(33, 220), sketch.random(93, 255));
        }

        // Next we define our methods
        display() {
          sketch.noStroke();
          sketch.fill(this.myCol);
          //note the use of our fields (variables)
          sketch.ellipse(this.x, this.y, this.dia + this.offDia, this.dia + this.offDia);
        }

        // A method to move the ball
        move() {
          this.x += this.vx;
          this.y += this.vy;
        }

        randomize() {
          let noiseOff = sketch.noise(sketch.frameCount+(this.x * 0.0015), sketch.frameCount+(this.y * 0.0015)) * 573;
          this.x += sketch.cos(noiseOff);
          this.y += sketch.sin(noiseOff);
        }

        oscillateDia(_freq, _amp) {
            this.offDia = sketch.sin(sketch.frameCount * _freq) * _amp;
        }


        // A method to test the ball's position
        check() {
          if (this.x < 0 + this.dia / 2) {
            //vx = vx * -1 (THIS MAKES ANY VALUE NEGATIVE).
            this.vx *= -1;
          }
          if (this.x > sketch.width - this.dia / 2) {
            this.vx *= -1;
          }
          if (this.y < 0 + this.dia / 2) {
            this.vy *= -1;
          }
          if (this.y > sketch.height - this.dia / 2) {
            this.vy *= -1;
          }
        }
      };
    };
    let myp52 = new p5(sketch2, 'sketch2');

    /*
    /////////////////////////////////////// Sketch 3 >>>
    let sketch3 = function( sketch ) {

      let myColour;
      let rectSize;

      sketch.setup = function() {
        sketch.createCanvas(680, 360);
        sketch.smooth();
        sketch.rectMode(sketch.CENTER);
        myColour = sketch.color(255);
        rectSize = 250;
      }

      sketch.draw = function() {
        sketch.background(0,0,33);
        sketch.fill(myColour);
        sketch.rect(sketch.width/2, sketch.height/2, rectSize, rectSize);

        sketch.infos(); // display textual information
      }

      /////////////////////////// FUNCTIONS ////////////////////////////
      // we use keyTyped function to add interaction with the keyboard
      // REF : https://p5js.org/reference/#/p5/keyTyped

      sketch.keyTyped = function() {
        if (sketch.key == 'r') {
          // Note the 3 local variables here
          // each storing a random value that is generated on each press of the key, 'r'

          let red = sketch.random(255); // random value between 0 et 255
          let green = sketch.random(255);
          let blue = sketch.random(255);
          // we then assign these random values to our colour variable
          myColour = sketch.color(red, green, blue);
          rectSize = sketch.random(100,320); // random value for size of shape
         }
      }

      // a function that displays textual info
      sketch.infos = function() {
       sketch.fill(255);
       sketch.textSize(18);
       sketch.text("Press r", 10, sketch.height-45);
       sketch.textSize(12);
       sketch.text("Chosen colour : " + myColour, 10, sketch.height-28);
       sketch.text("Chosen size : " + rectSize, 10, sketch.height-10);
      }
    };

    let myp53 = new p5(sketch3, 'sketch3');

    */

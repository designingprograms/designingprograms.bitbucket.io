/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: class_01
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/
let theBall;

function setup() {
  var canvas = createCanvas(680, 360);
  canvas.parent('canvas');
  background(33);
  smooth();
  noStroke();
  rectMode(CENTER);
  theBall = new Ball();

}

function draw() {
  background(33);

  //our methods
  theBall.display();
  theBall.move();
  theBall.check();


  fill(255);
  textSize(14);
  text("Press ' r '", 10, 290);
  text("Diameter: " + parseInt(theBall.dia) , 10, 310);
  text("SpeedX: " + parseInt(theBall.vx) , 10, 330);
  text("SpeedY: " + parseInt(theBall.vy) , 10, 350);
}

function keyTyped(){
  if(key == 'r'){
    setup();
  }
}

/////////////////////////// CLASS ////////////////////////////

/**
 * Here is the structure of our class
 *
 */

class Ball {

  // The constructor enables the creation of objects.
  // ***!IMPORTANT!***
  constructor(){
    // Our class fields (global variables that belong to our class)
    this.x = width/2;
    this.y = height/2;
    this.dia = random(50, 100);
    this.vx = random(-8, 8);
    this.vy = random(-10, 10);
  }

  // Next we define our methods
   display() {
    fill(255);
    //note the use of our fields (variables)
    ellipse(this.x, this.y, this.dia, this.dia);
  }

  // A method to move the ball
    move() {
    this.x+=this.vx;
    this.y+=this.vy;
  }


  // A method to test the ball's position
    check() {
    if (this.x<0 + this.dia/2) {
      //vx = vx * -1 (THIS MAKES ANY VALUE NEGATIVE).
      this.vx*=-1;
    }
    if (this.x>width - this.dia/2) {
      this.vx*=-1;
    }
    if (this.y<0 + this.dia/2) {
      this.vy*=-1;
    }
    if (this.y>height - this.dia/2) {
      this.vy*=-1;
    }
  }
}

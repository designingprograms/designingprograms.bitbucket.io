/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: function_02
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
 */
function setup() {
  var c2 = createCanvas(800, 360);
  c2.parent('canvas2');
  background(0, 0, 33);
  smooth();
  noStroke();

}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(255);
  // We declare a variable and assign it our function below.
  // This function returns a value that varies according to
  // an oscillation calculation
  var anime1 = oscillate(0.025, 273);
  var anime2 = oscillate(0.05, 273);
  var anime3 = oscillate(0.075, 273);

  // we then pass this variable as a parameter to our cross function
  // modifying the size of our cross
  fill(0,0,255);
  cross(215, height/2, anime1);
  fill(255,220,0);
  cross(430, height/2, anime2);
  push();
  translate(width/2+anime3, height/2);
  rotate(radians(anime3 * 0.73));
  fill(255,50,50);
  cross(0, 0, anime1);
  pop();

}

/**
 * This is a new function that returns a value
 * Again, we give it a name and define it's use within the curly brackets.
 * Notice the keyword 'return' which is used to send our value
 */
function oscillate(freq, amp) {
  var val = sin(frameCount*freq)*amp;
  return val;
}

function cross(_cntX, _cntY, _size) {
  rectMode(CENTER);
  noStroke();
  push();
  translate(_cntX, _cntY);
  rect(0, 0, _size, _size/4);
  rect(0, 0, _size/4, _size);
  pop();
}

/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: function_01
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/

let crossLen = 100;
let angle = 0;

function setup() {
  var c = createCanvas(680, 360);
  c.parent('canvas');
  background(0, 0, 33);
}


function draw() {
  background(0, 0, 33);
    fill(0, 0, 255);
    cross(150, 155, crossLen+150, 0);

    fill(150, 255, 0);
    cross(400, 155, 200, 45);

    fill(255, 200, 0);
    cross(575, 155, crossLen, angle);

    angle ++;
}

// Here is our function definition
// - It has a name.
// - Arguments for x, y, size and rotation angle.
// - And the definition between curly brackets.

function cross(_cntX, _cntY, _size, _angle) {
  rectMode(CENTER);
  noStroke();
  push();
  translate(_cntX, _cntY);
  rotate(radians(_angle));
  rect(0, 0, _size, _size/4);
  rect(0, 0, _size/4, _size);
  pop();
}

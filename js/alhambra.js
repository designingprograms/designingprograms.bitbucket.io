/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: loops_02
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/

let sketch1 = function(sketch) {

 /////////////////////////// SETUP ////////////////////////////
let numberOfSides;
let xInterval, yInterval;
let thickness;

/////////////////////////// SETUP ////////////////////////////
sketch.setup = function() {
  sketch.createCanvas(800, 420);
  sketch.background(0);
  sketch.smooth();
  sketch.strokeCap( sketch.SQUARE );
  numberOfSides = 4;
  xInterval = 50;
  yInterval = 50;
  thickness = 2.5;
}

/////////////////////////// DRAW ////////////////////////////
sketch.draw = function() {
  sketch.background(0);
  sketch.noFill();

    let polySize = sketch.map(sketch.mouseX, 0, sketch.width, xInterval/2, xInterval);
    let my = sketch.map(sketch.mouseY, 0, sketch.height, 0, sketch.TWO_PI);

    sketch.strokeWeight( thickness );

    for (var y = 75; y < sketch.height-50; y += yInterval) {
      for (var x = 75; x < sketch.width-50; x += xInterval) {
        sketch.push();
        sketch.translate(x, y);
        sketch.rotate(my);
        sketch.polygon(0, 0, numberOfSides, polySize,sketch.color(255));
        sketch.pop();

        sketch.push();
        sketch.translate(x, y);
        sketch.rotate(-my);
        sketch.polygon(0, 0, numberOfSides, polySize, sketch.color(255));//TRY DIFF VALUES AND *JIT NOT +JIT
        sketch.pop();
      }
    }
}

/////////////////////////// FUNCTIONS ////////////////////////////
// Function that draws a regular polygon

sketch.polygon = function( _x, _y, _numberOfSides, _sizes, _c) {

  let radius = _sizes;
  let angle = sketch.TWO_PI/_numberOfSides;
  sketch.stroke(_c);
  sketch.beginShape();
  sketch.push();
  sketch.translate(_x, _y);
  for (var i = 0; i<_numberOfSides; i++) {

    x = sketch.cos(angle*i) * radius;
    y = sketch.sin(angle*i) * radius;

    sketch.vertex(x, y);
  }
  sketch.endShape(sketch.CLOSE);
  sketch.pop();
}


// SOME KEY INTERACTIONS THAT ENABLE US TO CHANGE OUR VARIABLES
// AND THEREFORE CHANGE OUR VISUAL IN REAL TIME

sketch.keyTyped = function() {

  // Increase the size of our cirlces
  if (sketch.key === '+') {
    numberOfSides += 1;
  }

  // Decrease the size of our cirlces
  if (sketch.key === '-') {
    numberOfSides -= 1;
  }

  // Increase the y interval of our grid
  if (sketch.key === 'u') {
    yInterval += 2;
  }

  // Decrease the y interval of our grid
  if (sketch.key === 'j') {
    yInterval -= 2;
  }

  // Increase the x interval of our grid
  if (sketch.key === 'k') {
    xInterval += 2;
  }

   // Decrease the x interval of our grid
  if (sketch.key === 'h') {
    xInterval -= 2;
  }

  // Increase the thickness
  if (sketch.key === 'w') {
    thickness += 0.5;
  }

   // Decrease the x thickness
  if (sketch.key === 'x') {
    thickness -= 0.5;
  }
}

};

let myp5 = new p5(sketch1, 'sketch1');



///////////////////////////////////////////////////////////////////// Sketch 2 >>>
let sketch2 = function( sketch) {

  let margin;
  let interval;
  let size;

  /////////////////////////// SETUP ////////////////////////////
  sketch.setup = function() {
    sketch.createCanvas(720, 360);
    sketch.background(0);
    sketch.smooth();
    margin = 50;
    interval = 15;
    size = 10;
  }

  /////////////////////////// DRAW ////////////////////////////
  sketch.draw = function() {
    sketch.background(0);
    sketch.fill(255);
    for (var y=margin; y<sketch.height-margin; y+=interval) {
      for (var x=margin; x<sketch.width-margin; x+=interval) {
        //var s = sketch.noise(x * 0.005, y * 0.005, sketch.frameCount*0.005) * 75;
        sketch.ellipse(x, y, size, size);
      }
    }
  }
};

let myp52 = new p5(sketch2, 'sketch2');

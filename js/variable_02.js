/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: variable_02
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
 */

let x = 50;
let y = 253.078;

let theWord = "word";
let infos = "Press the 'b' key";
let theLetter = 'A';
let isDisplay = false;

let myc; // note: no intial value given.

function setup() {
  let canvas = createCanvas(680, 360);
  canvas.parent('canvas');
  background(33);
  smooth();
  noStroke();
}

function draw() {
  background(33);

  textSize(273);
  if(isDisplay) {
    myc = color(0,0,255);
    fill(myc);
    text(theWord, x, y);
  }else {
    myc = color(255,200,0);
    fill(myc);
    text(theLetter, x, y);
  }

  textSize(24);
  fill(255);
  text(infos, x, 310);
}

/////////////////////////// FUNCTIONS ////////////////////////////
// keyTyped is used in place of keyPressed() when we want
// to assign a particular key to an action.
// REF : https://p5js.org/reference/#/p5/keyTyped
function keyTyped(){
  if(key == 'b') {
    isDisplay = !isDisplay;
    //console.log(isDisplay);
  }
}

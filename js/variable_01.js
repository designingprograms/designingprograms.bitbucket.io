/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: variable_01
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/

let x = 0;
let dia = 0;

function setup() {
  var canvas = createCanvas(680, 360);
  canvas.parent('canvas');
  background(0,0,33);
  smooth();
  noStroke();
}

function draw() {
  background(0,0,33);
  fill(255);
  // Note how we use our variables.
  // We are passing them into our ellipse function as parameters
  ellipse(x, height/2, dia, dia);

  // here we are updating our variables
  x = x + 2;
  dia = dia + 0.5;

  fill(255);
  textSize(13);
  text("Press a key to restart animation", 10, height-25);

}

/////////////////////////// FUNCTIONS ////////////////////////////
function keyPressed(){
  background(0,0,33);
  x = 0;
  dia = 0;
}

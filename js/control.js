/**
* Designing Programs
* Web site: https://designingprograms.bitbucket.io
*
* Sketch: loops_01
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/
 /////////////////////////// SETUP ////////////////////////////
let sketch1 = function( sketch ) {

  let x = 0;
  let dia = 0;

  sketch.setup = function() {
    sketch.createCanvas(680, 360);
    sketch.background(0,0,33);
    sketch.smooth();
    sketch.noStroke();
  }

  sketch.draw = function() {
    sketch.background(0,0,33);
    sketch.fill(255);
    // Note how we use our variables.
    // We are passing them into our ellipse function as parameters
    sketch.ellipse(x, sketch.height/2, dia, dia);
    if(x>680 + dia/2){
      x = 0;
      dia = 0;
    }

    // here we are updating our variables
    x = x + 2;
    dia = dia + 0.5;

  };
};

let myp5 = new p5(sketch1, 'sketch1');

/////////////////////////////////////// Sketch 2 >>>
let sketch2 = function( sketch) {
  let myColour;

  sketch.setup = function() {
    sketch.createCanvas(680, 360);
    sketch.background(0,0,33);
    sketch.smooth();
    sketch.rectMode(sketch.CENTER);
    myColour = sketch.color(255);
  }

  sketch.draw = function() {
    sketch.background(0,0,33);

    sketch.fill(myColour);
    sketch.rect(sketch.width/2, sketch.height/2, 250, 250);

    sketch.infos(); // display textual information
  }

  /////////////////////////// FUNCTIONS ////////////////////////////
  // we use keyTyped function to add interaction with the keyboard
  // REF : https://p5js.org/reference/#/p5/keyTyped

  sketch.keyTyped = function() {
    if (sketch.key == 'a') {
       myColour = sketch.color(0, 0, 255);
     }

     if (sketch.key == 'b') {
       myColour = sketch.color(200, 255, 0);
     }

      if (sketch.key == 'c') {
       myColour = sketch.color(255, 0, 255);
     }
  }

  // a function that displays textual info
  sketch.infos = function() {
   sketch.fill(255);
   sketch.textSize(18);
   sketch.text("Press a | b | c", 10, sketch.height-30);
   sketch.textSize(12);
   sketch.text("Chosen colour : " + myColour, 10, sketch.height-12);
  }
};
let myp52 = new p5(sketch2, 'sketch2');

/////////////////////////////////////// Sketch 3 >>>
let sketch3 = function( sketch ) {

  let myColour;
  let rectSize;

  sketch.setup = function() {
    sketch.createCanvas(680, 360);
    sketch.smooth();
    sketch.rectMode(sketch.CENTER);
    myColour = sketch.color(255);
    rectSize = 250;
  }

  sketch.draw = function() {
    sketch.background(0,0,33);
    sketch.fill(myColour);
    sketch.rect(sketch.width/2, sketch.height/2, rectSize, rectSize);

    sketch.infos(); // display textual information
  }

  /////////////////////////// FUNCTIONS ////////////////////////////
  // we use keyTyped function to add interaction with the keyboard
  // REF : https://p5js.org/reference/#/p5/keyTyped

  sketch.keyTyped = function() {
    if (sketch.key == 'r') {
      // Note the 3 local variables here
      // each storing a random value that is generated on each press of the key, 'r'

      let red = sketch.random(255); // random value between 0 et 255
      let green = sketch.random(255);
      let blue = sketch.random(255);
      // we then assign these random values to our colour variable
      myColour = sketch.color(red, green, blue);
      rectSize = sketch.random(100,320); // random value for size of shape
     }
  }

  // a function that displays textual info
  sketch.infos = function() {
   sketch.fill(255);
   sketch.textSize(18);
   sketch.text("Press r", 10, sketch.height-45);
   sketch.textSize(12);
   sketch.text("Chosen colour : " + myColour, 10, sketch.height-28);
   sketch.text("Chosen size : " + rectSize, 10, sketch.height-10);
  }
};

let myp53 = new p5(sketch3, 'sketch3');

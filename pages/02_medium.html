<!DOCTYPE html>
<!--
/**

  /\  __-./\  ___\/\  ___\/\ \/\  ___\/\ "-.\ \/\ \/\ "-.\ \/\  ___\
  \ \ \/\ \ \  __\\ \___  \ \ \ \ \__ \ \ \-.  \ \ \ \ \-.  \ \ \__ \
   \ \____-\ \_____\/\_____\ \_\ \_____\ \_\\"\_\ \_\ \_\\"\_\ \_____\
    \/____/ \/_____/\/_____/\/_/\/_____/\/_/ \/_/\/_/\/_/ \/_/\/_____/

   ______ ______  ______  ______  ______  ______  __    __  ______
  /\  == /\  == \/\  __ \/\  ___\/\  == \/\  __ \/\ "-./  \/\  ___\
  \ \  _-\ \  __<\ \ \/\ \ \ \__ \ \  __<\ \  __ \ \ \-./\ \ \___  \
   \ \_\  \ \_\ \_\ \_____\ \_____\ \_\ \_\ \_\ \_\ \_\ \ \_\/\_____\

*
* Web site: https://designingprograms.bitbucket.io
*
* A Medium
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/ END -->
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="Designing Programs">
  <meta name="keywords" content="language, linguistics, Umberto Eco, computer language, programming, learning, algorithmic, semiotics">
  <meta name="author" content="Mark Webster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DP: Preface</title>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145817176-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145817176-2');
</script>


  <!--   Style  -->
  <link rel="stylesheet" href="../css/main.css">

  <!--   Scripts  -->
  <script language="javascript" type="application/javascript" src="../css/nav.js" />
  </script>

</head>

<body>
  <!-- burgerbars -->
  <div class="container" onclick="myFunction(this)">
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
  </div>

  <!-- sidenav -->
  <div id="mySidenav" class="sidenav">
    <a style="font-family:'IBMPlexMonoBold';font-size: 1.75em;color:black; " href="../index.html">HOME</a><br>
    <a href="00_preface.html">Preface</a>
    <a href="01_fundamentals.html">A Model</a>
    <a href="02_medium.html">A Medium</a>
    <a href="03_history.html">A History</a>
    <a href="04_variables.html">Variables</a>
    <a href="05_functions.html">Functions</a>
    <a href="06_control.html">Control</a>
    <a href="07_loops.html">Loops</a>
    <a href="08_class.html">Classes</a></br>

    <a style="font-family:'IBMPlexMonoLight';color:white; "href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="_blank">Source</a>

    <a style="font-family:'IBMPlexMonoLight';color:black; "href="../code/variable_01.html" target="_blank">Overview</a>
    </div>
  <!-- sidenav FIN -->

  <section id="main">
    <section class="body_center">
      <h1>A Medium</h1>
        <div class="citation_article">
          <span style="background:blue; color:white;">
          “Language lies at the very foundation of culture. </br>
          With regards to language, all other symbolic systems derive.”
          </span>
        </div>

    <p class="legend">Umberto Eco.<a href="http://www.signosemio.com/eco/modes-of-sign-production.asp" target="_blank"> Modes Of Sign Production.</a></p> </br></br>

          <!-- text center -------------------------->
          <h2>The Computer Language</h2>
            <div class="gridText1">
            <p>
              This chapter is an ultra concentrated introduction to computer language. I am unashamedly going to proceed in pure reductionism in order for us to understand language and brush aside the more fearful details of where linguistic studies can lead us - mostly into slippery, complex issues that go beyond my competence and the objectives of this resource. So if you are a hard-lined linguist, please accept my apologies in advance.
            </p>
            </div>

            <div class="gridText1">
            <p>
              Languages are systems of communication. We use them everyday to express ideas, thoughts, opinions, exchanging information, fact and observation. Each are based on a set of conventional rules that give the speaker as the listener, the reader as the writer, a common, collective and coherent system of sounds and symbols with which to communicate. Throughout the ages, languages have evolved and mutated into others, the conventions changing in line with the needs of the communities that use them. We could say then that language is fundamentally an organised and dynamic system of communication.
            </p>
            </div>

            <div class="gridText1">
            <p>
              It is organised in terms of having some logical structures, a syntax in linguistic parlance and a vocabulary that is common to those who use it. Language is dynamic in terms of possessing the characteristic for change and variation of those vocabularies and structures through the interaction of the groups who use it. Why we have so many of them is in part, if not mainly, based on how humans have evolved in different geographical environments and with quite different communication needs and cultural, political, economic and social values.
            </p>
            </div>

            <div class="gridText1">
            <p>
              Programming languages share similar characteristics with natural languages. They can equally be regarded as an organised and dynamic system of communication. They have also given birth, in their relatively short history, to a plethora of diverse languages: ALGOL, Fortran, C#, C++, Java, Lisp, SmallTalk, not forgetting the more esoteric LOLCODE, FALSE,<a href="https://fatiherikli.github.io/brainfuck-visualizer/"target="_blank"> Brainfuck</a> and even a language that mimics Shakespearean plays that is called, indeed,<a href="http://shakespearelang.sourceforge.net/report/shakespeare/" target="_blank"> Shakespeare.</a> These are just a small selection of literally hundreds of different computer languages that exist today.
            </p>
                <div class="sidenote">
                  Geneology of Programming Languages<br>
                  <a href="https://en.wikipedia.org/wiki/Generational_list_of_programming_languages" target="blank"><div id="sidenoteLink">A short list</div>
                  </a>
                  <a href="http://www.digibarn.com/collections/posters/tongues/tongues.jpg" target="_blank"><div id="sidenoteLink">Mother Tongues</div></a>
                  <a href="https://cdn.oreillystatic.com/news/graphics/prog_lang_poster.pdf" target="_blank"><div id="sidenoteLink">O'Reilly's diagram</div></a>
                  <a href="https://en.wikipedia.org/wiki/Esoteric_programming_language" target="_blank"><div id="sidenoteLink">Esoteric languages</div></a>

                  <!--<p>https://github.com/stereobooster/programming-languages-genealogical-tree</p>-->
                </div>
            </div>

            <div class="gridText1">
            <p>
              Very much like our human languages, computer languages have evolved through the exchange and development of a set of conventions creating a particular set of syntax and lexical terms. However, despite these similarities, these two systems of communication differ widely. The fundamental difference is that natural languages have evolved with human needs whereas programming languages have been designed primarily with machine needs.
            </p>
            </div>

            <div class="gridText1">
            <p>
            Due to the inherent logic of the computer, the underlying structures of a programming language reveal a strict set of limitations. Computer languages have a finite set of language constructs. That is to say, a finite vocabulary. Moreover the definitions for these are precise and complete. We can only instruct the computer to execute a certain task with a definite and precise set of language constructs.
            </p>
            </div>

            <div class="gridText1">
              <p>
                Furthermore, programming languages lack the semantic scope of natural ones. There is no possibility for ambiguity in the instructions we write in a computer language. There is no place for metaphor, irony, simile, or any other figure of speech. Put another way, the computer does not interpret a computer language like we interpret our own natural languages. Essentially, when we are programming, we are defining a procedure for the computer to execute. We are not seeking to express meaning with these language constructs, we are seeking to denote a strict rule set.
              </p>
            </div>

            <div class="gridText1">
            <p>
              This is equally true about a computer language’s syntax or grammar. The syntax is rigid with a few exceptions but these are rare. There can be no irregularity in the programming realm. Again, on a purely linguistic level, natural grammars are based on syntactical constructs that express meaning in the communication field between people. Whereas in formal computer language grammars, these structures denote precise instructions for the machine.
            </p>
            </div>

            <div class="gridText1">
            <p>
              I think it’s also important to note that computer languages are a one way affair. We do not dialogue with the computer, we do not talk to it and it doesn’t talk back or write back independently of some pre-programmed human input, even if the illusion of artificial intelligence can at times be very convincing. Sure, we have <a href="https://perlmonks.org/" target="_blank">Perl poetry</a>, <a href="https://en.wikipedia.org/wiki/Pseudocode" target="_blank">pseudo-code</a> and the odd geek meets geek at a conference who will embark on some bizarre exchange of code snippets. When it boils down to it though, programming is, for the moment at least, a textual affair with the machine.
            </p>
            </div>


              <p class="citation_article">
                “Code is not purely abstract and mathematical; it has significant social, political, and aesthetic dimensions.”
              </p>
              <p class="legend">Montfort Nick et al. 10 PRINT CHR$(205.5+RND(1)); : GOTO 10</p>


            <div class="gridText1">
              <p>
                Based on what I've just written, some may say that our everyday programmer is just an operator of functional input. However, within the wider scope of code and the emerging methodologies of critical software studies , we are beginning to understand  that this language system shares a dynamic and rich cultural dimension that has only just begun to take shape in our history. The wonderful book,<a href="http://nickm.com/trope_tank/10_PRINT_121114.pdf" target="_blank"> 10 PRINT CHR$(205.5+RND(1)); : GOTO 10</a> is a brilliantly written collaboration between academics in the field who focus on a single line of code, each deciphering in fine detail the extent of that succinct line of commands. This in-depth examination of code is testament to both its scope and influence as a medium in our contemporary societies.
              </p>
            </div>

            <div class="gridText1">
            <p>
              To bring this brief chapter to a close, I'd like to point out that learning to program is a difficult skill to acquire and do well. That said, and contrary to popular belief, actually learning the language is relatively simple in comparison to learning a natural language. This is something that I try to get across to my students early on because it crops up a lot and unfortunately in the form of some self-inflicted learning barrier. From what I have observed, it is not so much the language itself that poses problems for my students in the long run, rather it is understanding the mechanisms of that language and its scope of possibilities as a medium for creative practice.
            </p>
            </div>
      </br>

      <h2 style="background: black; max-width: 760px; text-align: right;" ><a href="03_history.html">Programming The Beautiful...</a></h2>


      <!-- Biblio -------------------------->
      </br>
      <h2>Notes</h2>


      <div class="notes">

        <!--Dyson George. <a href="https://www.edge.org/memberbio/george_dyson" target="_blank">Turing's Cathedral: The Origins of the Digital Universe</a></br>
        Random House, 2012</br></br>-->
        Everett Daniel. <a href="https://daneverettbooks.com/portfolio-item/language-the-cultural-tool/" target="_blank">Language: The Cultural Tool.</a></br>
        Profile Books, 2012</br></br>

        Gleick, James. <a href="http://around.com/the-information/" target="_blank">The Information</a></br>
        Fourth Estate, 2011</br></br>

        Graham Paul. <a href="http://www.paulgraham.com/langdes.html" target="_blank">Five Questions About Language Design.</a></br>
        Article, May 2001</br></br>

        Montfort Nick et al. <a href="http://nickm.com/trope_tank/10_PRINT_121114.pdf" target="_blank">10 PRINT CHR$(205.5+RND(1)); : GOTO 10.</a></br>
        MIT Press, 2013</br></br>

        Wikipedia contributors. <a href="https://en.wikipedia.org/wiki/Programming_language" target="_blank">Programming Language</a></br>
        Wikipedia, retrieved 14.02.2019</br></br>

        Temkin Daniel. <a href=" https://esoteric.codes/blog/sentences-on-code-art" target="_blank">Sentences On Code Art.</a></br>
        Article, December 2017</br></br>


      </div>
    </section>
    </br>


    <!-- The Footer -------------------------->
    <section class="body_center">
      <section class="footer">

        <h4>
          ©2019 copyright mark webster
        </h4>
        <p><a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="blank">BitBucket</a> |
          <a href="https://twitter.com/motiondesign_01" target="blank">Twitter</a> |
          <a href="mailto:mark.webster@wanadoo.fr?Subject=Hello%20again" target="_top">Contact</a>
        </p>
      </section>
    </section>

  </section>

</body>

</html>

<!DOCTYPE html>
<!--
/**

  /\  __-./\  ___\/\  ___\/\ \/\  ___\/\ "-.\ \/\ \/\ "-.\ \/\  ___\
  \ \ \/\ \ \  __\\ \___  \ \ \ \ \__ \ \ \-.  \ \ \ \ \-.  \ \ \__ \
   \ \____-\ \_____\/\_____\ \_\ \_____\ \_\\"\_\ \_\ \_\\"\_\ \_____\
    \/____/ \/_____/\/_____/\/_/\/_____/\/_/ \/_/\/_/\/_/ \/_/\/_____/

   ______ ______  ______  ______  ______  ______  __    __  ______
  /\  == /\  == \/\  __ \/\  ___\/\  == \/\  __ \/\ "-./  \/\  ___\
  \ \  _-\ \  __<\ \ \/\ \ \ \__ \ \  __<\ \  __ \ \ \-./\ \ \___  \
   \ \_\  \ \_\ \_\ \_____\ \_____\ \_\ \_\ \_\ \_\ \_\ \ \_\/\_____\

*
* Web site: https://designingprograms.bitbucket.io
*
* A Medium
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/ END -->
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="Designing Programs">
  <meta name="keywords" content="variables,Ada Lovelace, Charles Babbage, AnalyticalEngine, Processing, P5js, Learning, Algorithmic, Graphic, Lessons, computational,notebook,data, fundamentals">
  <meta name="author" content="Mark Webster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DP: Variables</title>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145817176-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145817176-2');
</script>


  <!--   Style  -->
  <link rel="stylesheet" href="../css/main.css">

  <!--   Scripts  -->
  <script language="javascript" type="application/javascript" src="../css/nav.js" />
  </script>
  <script language="javascript" type="application/javascript" src="../js/variable_01.js" />
  </script>

  <script type="text/javascript" src="../libs/p5.js"></script>
  <script type="text/javascript" src="../libs/p5.dom.js"></script>

</head>

<body>
  <!-- burgerbars -->
  <div class="container" onclick="myFunction(this)">
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
  </div>

  <!-- sidenav -->
  <div id="mySidenav" class="sidenav">
    <a style="font-family:'IBMPlexMonoBold';font-size: 1.75em;color:black; " href="../index.html">HOME</a><br>
    <a href="00_preface.html">Preface</a>
    <a href="01_fundamentals.html">A Model</a>
    <a href="02_medium.html">A Medium</a>
    <a href="03_history.html">A History</a>
    <a href="04_variables.html">Variables</a>
    <a href="05_functions.html">Functions</a>
    <a href="06_control.html">Control</a>
    <a href="07_loops.html">Loops</a>
    <a href="08_class.html">Classes</a></br>

    <a style="font-family:'IBMPlexMonoLight';color:white; "href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="_blank">Source</a>

    <a style="font-family:'IBMPlexMonoLight';color:black; "href="../code/variable_01.html" target="_blank">Overview</a>
    </div>
  <!-- sidenav FIN -->

  <section id="main">
    <section class="body_center">

      <h1>Variables</h1>
      <div class="citation_article">
        <span style="background:gold; color:black;">
          “...punches holes in a set of pasteboard cards in such a manner that when those cards
          are placed in a Jacquard loom, it will then weave upon its produce the exact pattern designed
          by the artist.”
        </span>
      </div>

      <p class="legend">Charles Babagge.</p> </br>

      <!-- text center -------------------------->
      <h2></h2>
      <div class="gridText1">
        <p>
          If we look back to the pioneering work of Charles Babbage and Ada Lovelace, we can trace the fundamental architecture of modern day computing; the concepts of <i>data storage</i> and <i>control structures.</i> Charles Babbage referred to
          these as the <i>Store</i>, where numbers are held and the <i>Mill</i> where arithmetic processing is performed. These were two distinct physical parts of the Analytical Engine, a machine that laid the foundations for both computing and
          programming. Up until the present day, little has changed in that architecture. Data is stored in memory (the <i>store</i>) and retrieved to apply operations on it (the <i>mill</i>).
        </p>
        <div class="sidenote">
          Historical Notes</br>
          <a href="http://history-computer.com/Babbage/AnalyticalEngine.html" target="_blank">
            <div id="sidenoteLink">The Analytical Engine</div>
          </a>
          <a href="https://www.youtube.com/watch?v=aBa7IBNSWp4" target="_blank">
            <div id="sidenoteLink">The Scientific Life of Ada Lovelace</div>
          </a>
          <a href="https://www.youtube.com/watch?v=FlfChYGv3Z4" target="_blank">
            <div id="sidenoteLink">The Greatest Machine That Never Was</div>
          </a>
        </div>
      </div>

      <div class="gridText1">
        <p>All computers execute programs in this manner. When we write a computer program, we are essentially defining and organising <i>data storage</i>* and <i>control structures</i>. So, one holds the stuff - numbers, words, images, sound etc. -
          and the other deals with processing it. This is a really important piece of information to grasp. It will help you not only in writing programs but also in analysing and understanding programs written by others. Furthermore, when you start
          creating your own data structures, you’ll find it far easier to think about the overall flow of your program from the outset. It means you will essentially be able to organise your code and have a much clearer vision of how to go about
          writing each step of a program.
        </p>
        <div class="sidenote">
          * I will use the term data structures in place of data storage from now on.
        </div>
      </div>

      <div class="gridText1">
        <p>One of the core ideas in the collaborative work between Babbage and Lovelace, was a concept they called variables. A variable is a means for storing data. We can store numbers, text, images, boolean states, colours, objects and almost any
          kind of data type we define. We can access that data from anywhere in our program, use it and more importantly modify it. Indeed, the real interest in variables is being able to modify them over time during the running of the program.
          Variables give us variable state. In the world of dynamic graphics, this means we can change shape, form, colour, position and whatever else we decide to modify.
        </p>
      </div>

      <div class="gridText1">
        <span class="input">INPUT</span>
      </div>
      <div class="gridText1">
        <p>There are two main steps to creating a variable:</br>
          1). Declare.</br>
          2). Assign.</br>
        </p>
      </div>

      <div class="gridText1">
        <p>To declare a variable, we often need to declare it’s data type. More will be written about this in the next chapter, suffice to say that there exist a handful of primitive data types which tell the computer what kind of data we are going
          to store. With a language like Processing, we must be explicit about those data types. Whereas with javaScript and hence P5js, the language is less strict on this matter.
        </p>
      </div>

      <div class="gridText1">
        <p>Along with the data type we must also give our variable a symbolic name. The name identifies the variable, the value of which is what we call an object, (more on the concept of objects later). The name we choose is entirely up to the
          programmer and it is usually chosen in accordance to it’s use. i.e. a variable’s name is logically chosen in line with what we intend it to be used for. For example, we may name a variable <i>'diameter'</i> because we would like to store a
          numeric value for modifying the size of a circle. As you start to read more code, you’ll begin to notice a number of conventions for naming variables and it’s good practice to follow suit. Clear code should be made as human readable as
          possible. In general, nouns are used for variable names and you’ll see why later on.
        </p>
      </div>

      <div class="gridText1">
        <p>The second step is to assign our variable with an initial value. This is a starting value which will change during the course of a program’s execution. It doesn't have to of course. This value could remain the same and this is what we
          call a constant. However, most variable values change during the running of a program. Which brings us to an essential part; updating the variable. We can apply simple operations such as adding, subtracting or multyplying to modify our
          variable values. It is also entirely possible to apply operations on multiple variables and indeed, as we move on, you'll see that variables are used to modify parameters, or what we call arguments, for more complex structures, like
          functions for example.
        </p>
      </div>

      <div class="gridText1">
        <span class="syntax">SYNTAX</span>
      </div>
      <div class="gridText1">
        <code>
          <span class="red"> data type </span>&nbsp;
          <span class="green">&lsaquo;variable name&rsaquo;</span>=
          <span class="blue"> &lsaquo;variable value&rsaquo;</span></br></br></br>

          Eg 1. js. *</br>
          <span class="redCode">let</span>
          <span class="greenCode">x</span> =
          <span class="blueCode">0;</span></br></br>

          Eg 2. java</br>
          <span class="redCode">float</span>
          <span class="greenCode">x</span> =
          <span class="blueCode">0;</span></br>

        </code>
        <div class="sidenote"><br><br>
          * We are declaring <br>& assigning our variables all on one line.
        </div>
      </div></br></br>
      <div class="gridText1">
        <span class="output">OUTPUT</span>
      </div>

      <!-- CANVAS P5JS -------------------------->
      <div id="canvas"> </div></br>

      <div class="gridText1">
        <code>
          // here we are updating our variables</br>
          <span class="blackCode">x = x + 2;</span></br>
          <span class="blackCode">dia = dia + 0.5;</span></br></br>
        </code>
      </div>
      </br>

      <div class="gridText1">
        <!-- add later
        <span class="observe"><a href="../code/variable_01.html"target="_blank"> Interactive version & source.</a></span>
      -->
        <h2>Details</h2>
      </div>
      <div class="gridText1">
        <p>The variable is an essential concept yet it takes a little time to understand the full workings for the beginner. One of the reasons for this, is that the student doesn’t immediately grasp the flow of data within a program. It can be
          difficult at first to trace where we update the value of a variable and also how we use that variable within a function for example. When we start to develop more complex programs, understanding what updates a variable is not always so
          explicit. For example, a simple operation like <i>dia = dia + 1;</i> is easily traced – the value of 1. However in <i>dia = dia * speed;</i> the value of speed is stored in another variable and we’d need to read a little more of the code
          to see where this variable is declared and understand eventually the values it produces.
        </p>
      </div>

      <div class="gridText1">
        <p>
          There are slight variations in the manner we declare and eventually assign variables. For example, we can both declare and assign all on one line, as we have done above. However, it is equally possible to simply declare a variable in one
          part of our program and then assign it a value elsewhere. The best way to get accustomed to these conventions is to start reading simple programs oneself. In the next chapter, we'll look at primitive data types, the diffrence between
          strongly typed languages such as Processing and weakly typed ones such as JavaScript. We'll also look at a concept called variable scope within a program.
        </p>
      </div>

      <div class="gridText1">
        <span class="observe"><a href="https://observablehq.com/@motiondesign_01/variables" target="_blank"> Observe: explore this concept further >>></a></span>
      </div>
    </section> </br>

    <!-- wide image -------------------------->
    <section class="main_wide">
      <img src="../media/naturalForm.png" alt="IMG">
      <p class="legend">Variables observed in nature.Source: <a href="https://www.flickr.com/photos/britishlibrary" target="_blank">British Library</a> </p> </br>
    </section>
    <section class="body_center">
      <h2 style="background: black; max-width: 760px; text-align: right;"><a href="04b_variables_type.html">data types...</a></h2>
    </section>

    <!-- Biblio -------------------------->
    <section class="body_center">
      </br>
      <h2>Notes</h2>

      <div class="notes">
        Padua, Sydney. <a href="http://sydneypadua.com/2dgoggles/the-book/" target="_blank">The Thrilling Adventures of Lovelace & Babbage</a></br>
        Penguin Books 2015</br></br>

        Sécheret, Anne.<a href="https://www.franceculture.fr/emissions/les-cours-du-college-de-france/decouverte-fondamentale-invention-technologique-innovation-un-voyage-scientifique-17-la-saga-du" target="_blank">Découverte fondamentale, invention technologique, innovation : un voyage scientifique (1/7) : La saga du numérique : de la machine à calculer à l’invention du transistor</a></br>
        France Culture 2017</br></br>

        Webber, Jordan Erica.<a href="https://www.theguardian.com/technology/audio/2019/oct/07/the-lovelace-effect-ada-lovelace-chips-with-everything-podcast" target="_blank">The Lovelace effect: Chips with Everything podcast</a></br>
        The Guardian 07.10.2019</br></br>

        

      </div>

    </section>
  </br>

    <!-- The Footer -------------------------->
    <section class="body_center">
      <section class="footer">
        <h4>
          ©2019 copyright mark webster
        </h4>
        <p><a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="blank">BitBucket</a> |
          <a href="https://twitter.com/motiondesign_01" target="blank">Twitter</a> |
          <a href="mailto:mark.webster@wanadoo.fr?Subject=Hello%20again" target="_top">Contact</a>
        </p>
      </section>
    </section>

  </section>
</body>

</html>

<!DOCTYPE html>
<!--
/**

  /\  __-./\  ___\/\  ___\/\ \/\  ___\/\ "-.\ \/\ \/\ "-.\ \/\  ___\
  \ \ \/\ \ \  __\\ \___  \ \ \ \ \__ \ \ \-.  \ \ \ \ \-.  \ \ \__ \
   \ \____-\ \_____\/\_____\ \_\ \_____\ \_\\"\_\ \_\ \_\\"\_\ \_____\
    \/____/ \/_____/\/_____/\/_/\/_____/\/_/ \/_/\/_/\/_/ \/_/\/_____/

   ______ ______  ______  ______  ______  ______  __    __  ______
  /\  == /\  == \/\  __ \/\  ___\/\  == \/\  __ \/\ "-./  \/\  ___\
  \ \  _-\ \  __<\ \ \/\ \ \ \__ \ \  __<\ \  __ \ \ \-./\ \ \___  \
   \ \_\  \ \_\ \_\ \_____\ \_____\ \_\ \_\ \_\ \_\ \_\ \ \_\/\_____\

*
* Web site: https://designingprograms.bitbucket.io
*
* A Medium
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/ END -->
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="Designing Programs">
  <meta name="keywords" content="Classes, objects, encapsulation, data abstraction, graphic design, Design, Algorithmic, computational">
  <meta name="author" content="Mark Webster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DP: Class II</title>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145817176-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145817176-2');
</script>


  <!--   Style  -->
  <link rel="stylesheet" href="../css/main.css">

  <!--   Scripts  -->
  <script language="javascript" type="application/javascript" src="../css/nav.js" />
  </script>

  <script type="text/javascript" src="../libs/p5.js"></script>
  <script type="text/javascript" src="../libs/p5.dom.js"></script>
  <script language="javascript" type="application/javascript" src="../js/class_02b.js" />
  </script>

</head>

<body>
  <!-- burgerbars -->
  <div class="container" onclick="myFunction(this)">
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
  </div>


  <!-- sidenav -->
  <div id="mySidenav" class="sidenav">
    <a style="font-family:'IBMPlexMonoBold';font-size: 1.75em;color:black; " href="../index.html">HOME</a><br>
    <a href="00_preface.html">Preface</a>
    <a href="01_fundamentals.html">A Model</a>
    <a href="02_medium.html">A Medium</a>
    <a href="03_history.html">A History</a>
    <a href="04_variables.html">Variables</a>
    <a href="05_functions.html">Functions</a>
    <a href="06_control.html">Control</a>
    <a href="07_loops.html">Loops</a>
    <a href="08_class.html">Classes</a></br>

    <a style="font-family:'IBMPlexMonoLight';color:white; "href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="_blank">Source</a>

    <a style="font-family:'IBMPlexMonoLight';color:black; "href="../code/variable_01.html" target="_blank">Overview</a>
    </div>
  <!-- sidenav FIN -->

  <section id="main">
    <section class="body_center">
      <h1>
        Class II
      </h1>
      <div class="citation_article">
        <span style="background:Indigo; color:white;">
          “Computer programs are good, they say, for particular purposes, but they aren’t flexible. Neither is a violin, or a typewriter, until you learn how to use it.”
        </span>
      </div>

      <p class="legend">Marvin Minsky. <a href="
      https://web.media.mit.edu/~minsky/papers/Why%20programming%20is--.html" target=_blank>Why Programming Is a Good Medium for </br>Expressing Poorly-Understood and Sloppily-Formulated Ideas.</a>
      </p> </br></br>

      <!-- text center -------------------------->
      <h2>One Class, Many Objects</h2>
      <div class="gridText1">
        <p>
          Classes help us organise complex ideas, making it easier to maintain our code and giving us greater flexibility in terms of what we can achieve in this medium. When using classes, we talk about creating an object. You can consider classes
          as a blueprint if you like and an object is one example or instance of that blueprint. What is important to grasp is that each object that we create is a unique entity of that class.
        </p>
      </div>

      <div class="gridText1">
        <p>
          Objects are characterised by three essential properties: <i>state, identity and behaviour</i>. The state of an object is a given value determined by the data fields of that class. The identity of an object distinguishes one object from
          another. It is useful to think of an object’s identity as a place where its value is stored in memory. The behaviour of an object is the effect of the class methods and hence the operations performed on the data.
        </p>
      </div>

      <div class="gridText1">
        <p>
          Again, these principles echo back to what <a href="01_fundamentals.html">I mentioned at the beginning of this course,</a> that a computer program is essentially made up of data structures and control structures. Data stores symbolic values
          whilst control structures deal with processing that data. With classes, we now have the means to define our own data types and furthermore encapsulate both data structures and control structures all in one neat entity.
        </p>
      </div>

      <div class="gridText1">
        <p>
          The world of object-oriented programming or OOP for short, is really the next level up when it comes to programming with JavaScript or Processing. There are many advanced concepts linked with OOP and I’ve noted a few references below for those who may wish to delve into the more profound areas. That said, I
          rarely teach classes beyond its basic syntax and principle of encapsulation. I don’t expect either for my students to necessarily dive too deep into OOP. Being able to use classes from a third party library is already a big step for most.
          If you have understood how to use a simple class, then that’s great.
        </p>
      </div>

      <div class="gridText1">
        <p>
          Let's take our last example for a bouncing ball and create another instance of this class. We are going to add another method to our class too that will give it a different behaviour. The idea is to understand how we can create numerous
          objects of the same class and attribute different behaviours depending on the methods we call upon them. From here, it should be clear that this way of programming gives a certain level of organisation and flexibility. We have a single
          entity to which we can add further methods and data fields. The class therefore becomes a modular structure that we can eventually build on.
        </p>
      </div></br>

      <div class="gridText1">
        <code>

          Eg 1. JavaScript</br>

          <span class="blackCode">let</span>
          <span class="blueCode"> ball_01;</br>
            <span class="blackCode">let</span>
            <span class="blueCode"> ball_02;</br>
              ...</br>
              <span class="blackCode">// notice we have added arguments to our constructor</br>
                <span class="blueCode">ball_01</span> <span class="blackCode">= new Ball(200, 180);</br>
                  <span class="blueCode">ball_02</span> <span class="blackCode">= new Ball(450, 180);</br>
                    ...</br>
                    <span class="blackCode">// notice we can also have arguments for our methods</br>
                      <span class="blackCode">// this enables us to pass in different values.</br>
                        <span class="blueCode">ball_01.
                          <span class="goldCode">
                            oscillateY(0.035, 90);</br>
                            <span class="blueCode">ball_01.
                              <span class="goldCode">
                                oscillateX(0.095, 60);</br>
                                <span class="blackCode">
                                  ...</br>

                                  <span class="blueCode">ball_02.
                                    <span class="goldCode">
                                      oscillateDia(0.05, 135);</br>
                                      <span class="blackCode">
                                        ...</br>
        </code>
        <div class="sidenote">
          <p>
            You can execute this sketch with <a href="https://editor.p5js.org/designingprograms/sketches/akdcui3A1" target="_blank">P5js's online editor</a>
            </p>


        </div>
      </div>

      <div class="gridText1">
        <span class="output">OUTPUT</span>
      </div>

      <!-- CANVAS P5JS -------------------------->
      <div id="sketch1"> </div></br>

      <div class="gridText1">
        <p>
          Not all the code is apparent above, so please check out the source and explore this sketch. To summarise, we have two objects declared and we are calling different methods on each as well as passing in different values as arguments for these methods. We have already seen this concept of passing in data with functions. This is exactly the same thing. Methods are after all just functions that belong to a class. Objects are dynamic, meaning we can make changes to them at run time. Notice what happens when you press the 'm' key on your keyboard. If you look closely at the source code, you'll realise we are changing method calls on our objects.
        </p>

      </div>

      <div class="gridText1">
        <p>
          To drive home the concept of objects and the flexibility of classes, let us look at an example that creates many objects. To do this we are going to create an array or list of objects.
        </p>
      </div></br>

      <div class="gridText1">
        <code>
          Eg 1. JavaScript</br>

          <span class="blackCode">let</span>
          <span class="blueCode"> theBalls = []; // an array of objects</br>
            ...</br>
            <span class="blackCode">
              // to instantiate our array of objects, we use a FOR loop</br>
              &nbsp;&nbsp;&nbsp;for(var i=0; i<200; i++){ </br> &nbsp;&nbsp;&nbsp;&nbsp; theBalls[i]=new Ball();</br> } ...</br> </br> </br> <span class="blackCode">
                // and to call our methods on our objects, we also use a loop</br>
                &nbsp;&nbsp;&nbsp;for(var i=0; i<200; i++){ </br> &nbsp;&nbsp;&nbsp;&nbsp; theBalls[i].display();</br> &nbsp;&nbsp;&nbsp;&nbsp; theBalls[i].move();</br> &nbsp;&nbsp;&nbsp;&nbsp; theBalls[i].check();</br> } ...</br> </br> </br> </code>
                  </div> <div class="gridText1">
                  <span class="output">OUTPUT</span>
      </div>

      <!-- CANVAS P5JS -------------------------->
      <div id="sketch2"> </div></br>



      <div class="gridText1">
        <p>
          This wraps up this chapter and it is also where I end this concise introduction to programming. My main aim with this website and brief course has been to explain the five core concepts: <i>variables, functions, control, loops and classes.</i> I wanted to give some insight into the mechanisms and structures that underlie most modern programming languages, helping the beginner to understand these in a clear manner without going into too much detail, nor diverging into the formal capacities of code. I hope I have managed to do that. If you are a reader from afar who has stumbled upon this course (which I wrote to accompany my teaching course), and you have any suggestions or would like to point out mistakes or confusing passages, please do not hesitate to contact me. I'd be most appreciative of feedback.
        </p>

      </div>

      <div class="gridText1">
        <p>
          From here, the student is encouraged to pursue their exploration of this medium with further readings and personal reasearch in the many artistic projects that exist out there. You are also encouraged to go beyond the context of code as just purely a creative medium. Code, in its wider sense, is a culturally important media and medium shaping many aspects of our current landscape. It is quickly shaping not only form but also ideas and concepts, thoughts and behaviours. The scope of code is far reaching and fundamentally an essential part of contemporary society. Learning to read and write it has already become a vital part of education and it will continue to enrich our understanding. Code is language and I don't know of any aspect of our life that isn't concerned with language. It's all around and within us. Embrace it.
        </p>

      </div>

    </section>


    <!-- Biblio -------------------------->
    <section class="body_center">
      </br>
      <h2>Notes</h2>

      <div class="notes">

        Kyuha Shim. <a href=" https://medium.com/@qshim/computation-for-graphic-designers-23629ec63dc0#.wtu8r4afx">Computation for Graphic Designers. 2016</a></br></br>

        Chaitanya Singh. <a href="https://beginnersbook.com/2013/04/oops-concepts/">Object-Oriented Programing Concepts in Java </a></br>
        Mozilla Foundation. 2019</br></br>


        Javin Paul. <a href="https://hackernoon.com/10-oop-design-principles-every-programmer-should-know-f187436caf65">10 OOP Principles Every Programmer Should Know.</a></br>
        Hackernoon</br></br>

      </div>

    </section>
    </br>

    <!-- The Footer -------------------------->
    <section class="body_center">
      <section class="footer">
        <h4>
          ©2019 copyright mark webster
        </h4>
        <p><a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="blank">BitBucket</a> |
          <a href="https://twitter.com/motiondesign_01" target="blank">Twitter</a> |
          <a href="mailto:mark.webster@wanadoo.fr?Subject=Hello%20again" target="_top">Contact</a>
        </p>
      </section>
    </section>

  </section>
</body>

</html>

<!DOCTYPE html>
<!--
/**

  /\  __-./\  ___\/\  ___\/\ \/\  ___\/\ "-.\ \/\ \/\ "-.\ \/\  ___\
  \ \ \/\ \ \  __\\ \___  \ \ \ \ \__ \ \ \-.  \ \ \ \ \-.  \ \ \__ \
   \ \____-\ \_____\/\_____\ \_\ \_____\ \_\\"\_\ \_\ \_\\"\_\ \_____\
    \/____/ \/_____/\/_____/\/_/\/_____/\/_/ \/_/\/_/\/_/ \/_/\/_____/

   ______ ______  ______  ______  ______  ______  __    __  ______
  /\  == /\  == \/\  __ \/\  ___\/\  == \/\  __ \/\ "-./  \/\  ___\
  \ \  _-\ \  __<\ \ \/\ \ \ \__ \ \  __<\ \  __ \ \ \-./\ \ \___  \
   \ \_\  \ \_\ \_\ \_____\ \_____\ \_\ \_\ \_\ \_\ \_\ \ \_\/\_____\

*
* Web site: https://designingprograms.bitbucket.io
*
* Main index
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/ END -->
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="Designing Programs">
  <meta name="keywords" content="Computational Design, Design, Learning, Algorithmic, Graphic, Lessons, computational, notebook, graphic design, fundamentals">
  <meta name="author" content="Mark Webster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DP: A Model For Thought</title>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145817176-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145817176-2');
</script>


  <!--   Style  -->
  <link rel="stylesheet" href="../css/main.css">

  <!--   Scripts  -->
  <script language="javascript" type="application/javascript" src="../css/nav.js" /></script>

</head>

<body>
  <!-- burgerbars -->
  <div class="container" onclick="myFunction(this)">
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
  </div>

  <!-- sidenav -->
  <div id="mySidenav" class="sidenav">
    <a style="font-family:'IBMPlexMonoBold';font-size: 1.75em;color:black; "href="../index.html">HOME</a><br>
    <a href="00_preface.html">Preface</a>
    <a href="01_fundamentals.html">A Model</a>
    <a href="02_medium.html">A Medium</a>
    <a href="03_history.html">A History</a>
    <a href="04_variables.html">Variables</a>
    <a href="05_functions.html">Functions</a>
    <a href="06_control.html">Control</a>
    <a href="07_loops.html">Loops</a>
    <a href="08_class.html">Classes</a></br>

    <a style="font-family:'IBMPlexMonoLight';color:white; "href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="_blank">Source</a>

    <a style="font-family:'IBMPlexMonoLight';color:black; "href="../code/variable_01.html" target="_blank">Overview</a>
    </div>
    <!-- sidenav FIN -->

  <section id="main">
  <section class="body_center">

      <h1>A Model For Thought</h1>
        <div class="citation_article">
          <span style="background:blue; color:white;">
          “Every computer program is a model, hatched in the mind, of a real or mental process.
          These processes, arising from human experience and thought are huge in number,
          intricate in detail, and at any time only partially understood.”
          </span>
        </div>

    <p class="legend">Alan J. Perlis.<a href="http://web.mit.edu/alexmv/6.037/sicp.pdf" target="_blank"> Structure & Interpretation of Computer Programs</a></p> </br></br>

  <!-- text center -------------------------->
  <h2>Organising The Particulars</h2>
    <div class="gridText1">
    <p>
      Computers are essentially clocks ticking over a multitude of tasks, shifting and modifying data around at amazing speed. To do this, they execute programs; a pattern of rules that execute pre-determined operations on a set of data. Since the arrival of desktop computers in the early eighties, the interface of the command line and that of programming has been mostly rendered opaque. Nowadays, the majority of us manipulate graphical interfaces on the computer and so rarely get to understand these mechanisms. Programming gives us access to a lower-level interface with the machine. One that gives us the possibility to write our own programs and eventually define both the data and data processes within those programs. And that is a powerful paradigm shift for anyone interested in harnessing the full potential of the machine.
    </p>
    </div>

    <div class="gridText1">
      <p>A computer program is a written text that defines a process and there are three key parts that make up those processes. At the highest level, we have thinking, without which our ideas could not take form and a process can not be formulated. In the realm of programming we define our thinking through algorithms - a method for describing a formal set of rules. In order to write these algorithms we need the help of two other vital parts that constitute the bare bones of a program: Data and control structures. Data stores symbolic values - a number, a name, an image, a vector, a colour… - whilst control structures deal with processing that data.
      </p>
  </div>

  <div class="gridText1">
      <p>The term structure is important here because essentially a process is about things that are related to other things. We are concerned therefore with the relationships in a program between each component. Being able to understand those relationships is vital if ever you want to really get to grips with writing your own programs and translating thoughts into ideas that become fully working programs that implement those thoughts. Structure is what gives thoughts the possibility to evolve and this on all levels of any process. A computer program gives us the environment and the medium to express our ideas, save them, execute them at any time and indeed share them.
      </p>
  </div>

  <div class="gridText1">
      <p>While most students are capable of understanding what a program is and playing with the code. i.e. exploring a program written by someone else, changing some values and observing what happens, they find it hard to actually write something from scratch. Putting their own ideas into application through the medium of writing code is a challenge. What I have observed over the years with my students is their difficulty in understanding the various structures of a program. At first hand, it is the visual structure, getting to grips with the syntax and lexical elements of the language. Secondly, it is the logical structure, getting to grips with the flow of data and the various operations that modify that data.
      </p>
  </div>

  <div class="gridText1">
    <p>Indeed, one of the major hurdles for many visual artists who are used to manipulating graphical interfaces on the computer, is in getting them to understand the workings of a purely textual and hence more abstract one. Part of my goal in this course is to unveil the mystery of those abstract structures and reveal some of their possibilities within the area of graphics.
    </p>
  </div>

  <h2>Five Core Concepts</h2>
  <p style="font-size: 1.6em;"><a href="04_variables.html"> Variables</a> |
      <a href="05_functions.html">Functions</a> |
      <a href="06_control.html">Control</a> |
        <a href="07_loops.html">Loops</a> |
          <a href="08_class.html">Classes</a></p>

  <div class="gridText1">
      <p>I divide my course into the teaching of five core concepts that underly most modern programming languages. Each concept is presented and explained in a clear and simple manner. I've tried to give some historical context too but the main focus is on understanding how these concepts can be applied to practical use in developing visual tools. It must be said that this is a concise course and it is not my intention to tackle the finer details of each. To go deeper, I've added notes and references.</p>
  </div>

  <div class="gridText1">
      <p>As part of my teaching, each concept is followed up with a variety of exercises that encourage the student to explore the concept as well as understand its practical use. I will not go into any detail about these exercises as it is not my main aim here. However, I will occasionaly use some student work to illustrate these pages. For example, the image below is a mosaic of student work that resulted from a first excercise on gestalt theory.
      </p>
      <div class="sidenote">
        <p>
          Student Work<br>
          <a href="https://www.are.na/mark-webster-1526199907/designing-programs-student-work">Books & posters.</a><br>
          <a href="https://www.are.na/mark-webster-1526199907/typography-students">Typography.</a>
      </p>
      </div>
    </div>

   </br>

</section>

   <!-- wide image -------------------------->
     <section class="main_wide">
       <img src="../media/ex_01.jpg" alt="IMG">
       <p class="legend">A mosaic of student work on Gestalt theory.</p>
     </section>

<section class="body_center">
  <h2>Source Code & Examples Online</h2>
  <div class="gridText1">
      <p>All source code for this course can be consulted at the course bitbucket repository*. All examples are written for Processing and for P5js and can therefore be consulted directly in the browser without having to install or compile anything on your machines. I also use the wonderful platform, <a href="https://beta.observablehq.com/@motiondesign_01/setting-up-p5js"target="blank">Observable</a>, developed by Mike Bostock, in order to develop on finer details. The beauty of Observable is that we can both explain and explore code in an intuitive manner. As you will see in later chapters, Observable is a powerful tool that I’m most grateful to use in my teachings. </p>

      <div class="sidenote">
        <p>
          * BitBucket<br>
          <a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="
                  ">All Source Code is here.</a>
      </p>
      </div>
  </div>

  <div class="gridText1">
      <p>If you would like to see what we are going to study, then you can access all five concepts of the concepts at the link in the side note. You can also find this link in the menu side bar at Overview. The aim of this is to give quick and easy access to code and sketches that appear throughout the course. You will find helpful links to the respective chapters and Observable pages.</p>

      <div class="sidenote">
      <p>
        An Overview<br>
        <a href="../code/variable_01.html" target="
                ">See all 5 concepts</a>
    </p>
      </div>
  </div>
</br>
    <h2 style="background: black; max-width: 760px; text-align: right;" ><a href="02_medium.html">A Medium...</a></h2>

  <!-- Biblio -------------------------->
  </br>
  <h2>Notes</h2>

  <div class="notes">
    Downey Allen. <a href="https://blogs.scientificamerican.com/guest-blog/programming-as-a-way-of-thinking/?ref=quuu&utm_source=quuu" target="_blank">Programming as a Way of Thinking.</a></br>
    Scientific American. 26 April 2017</br></br>

    Lévy Pierre. <a href="http://www.sens-public.org/article1275.html?lang=fr" target="_blank">La pyramide algorithmique.</a></br>
    Blog post. 15 December 2017</br></br>

    Abiteboul Serge, Dowek Gilles. <a href="https://www.editions-lepommier.fr/le-temps-des-algorithmes" target="_blank">Le temps des algorithmes.</a></br>
    Le Pommier, 2013</br></br>

    Graham Paul. <a href="http://www.paulgraham.com/hp.html" target="_blank">Hackers &amp; Painters.</a></br>
    Article. May 2003</br></br>

    Teddre Matti, Denning Peter J. <a href="http://denninginstitute.com/pjd/PUBS/long-quest-ct.pdf" target="_blank">The Long Quest for Computational Thinking.</a></br>
    Article. 2016</br></br>

    McCormack John et al. <a href="http://jonmccormack.info/wp-content/uploads/2012/10/TenQuestionsV3.pdf" target="_blank">Ten Questions Concerning Generative Computer Art.</a></br>
    Article. April 2012</br></br>




  </div>

</section>
</br>

  <!-- The Footer -------------------------->
  <section class="body_center">
      <section class="footer">
        <div class="footerGrid">
          <h4>
          ©2019 copyright mark webster
        </h4>
          <p><a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/"target="blank">BitBucket</a> |
            <a href="https://twitter.com/motiondesign_01" target="blank">Twitter</a> |
            <a href="mailto:mark.webster@wanadoo.fr?Subject=Hello%20again" target="_top">Contact</a>
          </p>
        </div>
      </section>
  </section>

</section>
</body>

</html>

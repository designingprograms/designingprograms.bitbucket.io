<!DOCTYPE html>
<!--
/**

  /\  __-./\  ___\/\  ___\/\ \/\  ___\/\ "-.\ \/\ \/\ "-.\ \/\  ___\
  \ \ \/\ \ \  __\\ \___  \ \ \ \ \__ \ \ \-.  \ \ \ \ \-.  \ \ \__ \
   \ \____-\ \_____\/\_____\ \_\ \_____\ \_\\"\_\ \_\ \_\\"\_\ \_____\
    \/____/ \/_____/\/_____/\/_/\/_____/\/_/ \/_/\/_/\/_/ \/_/\/_____/

   ______ ______  ______  ______  ______  ______  __    __  ______
  /\  == /\  == \/\  __ \/\  ___\/\  == \/\  __ \/\ "-./  \/\  ___\
  \ \  _-\ \  __<\ \ \/\ \ \ \__ \ \  __<\ \  __ \ \ \-./\ \ \___  \
   \ \_\  \ \_\ \_\ \_____\ \_____\ \_\ \_\ \_\ \_\ \_\ \ \_\/\_____\

*
* Web site: https://designingprograms.bitbucket.io
*
* Main index
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/ END -->
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="Designing Programs">
  <meta name="keywords" content=", Design, Learning, Algorithmic, Graphic, Lessons, Preface, programming course">
  <meta name="author" content="Mark Webster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DP: Preface</title>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145817176-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145817176-2');
</script>


  <!--   Style  -->
  <link rel="stylesheet" href="../css/main.css">

  <!--   Scripts  -->
  <script language="javascript" type="application/javascript" src="../css/nav.js" />
  </script>

</head>

<body>
  <!-- burgerbars -->
  <div class="container" onclick="myFunction(this)">
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
  </div>

  <!-- sidenav -->
  <div id="mySidenav" class="sidenav">
    <a style="font-family:'IBMPlexMonoBold';font-size: 1.75em;color:black; " href="../index.html">HOME</a><br>
    <a href="00_preface.html">Preface</a>
    <a href="01_fundamentals.html">A Model</a>
    <a href="02_medium.html">A Medium</a>
    <a href="03_history.html">A History</a>
    <a href="04_variables.html">Variables</a>
    <a href="05_functions.html">Functions</a>
    <a href="06_control.html">Control</a>
    <a href="07_loops.html">Loops</a>
    <a href="08_class.html">Classes</a></br>

    <a style="font-family:'IBMPlexMonoLight';color:white; "href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="_blank">Source</a>

    <a style="font-family:'IBMPlexMonoLight';color:black; "href="../code/variable_01.html" target="_blank">Overview</a>
    </div>
  <!-- sidenav FIN -->

  <section id="main">
    <section class="body_center">

      <h1> Preface </h1>

        <div class="citation_article"> <span style="background:blue; color:white;"> “To describe the problem is part of the solution. </br>This implies:not to make creative decisions as prompted by feeling but by intellectual criteria.” </span> </div>
        <p class="legend">Karl
          Gerstner. Designing Programmes. 1964</p></br></br>

      <h2>Introduction to Designing Programs</h2>
      <div class="gridText1">
        <p>
          <i>Designing Programs</i> is a condensed, pedagogical course to computer programming for higher education students working in the visual arts. It serves both as a means for learning how to program and for understanding how the medium of
          code can be used in the creative process. It follows a structured and methodological approach to acquiring fundamental concepts of programming. I have kept it concise, focusing on the technicalities yet giving reference to a vast
          number of texts, projects and resources that I hope will enable the learner to deepen his or her knowledge on the subject.
        </p>
      </div>
      <div class="gridText1">
        <p>
          <i>Designing Programs</i> is the fruit of working with a variety of visual artists, designers and schools as well as giving many workshops and lectures on the subject since 2010. The present form has evolved in line with my most recent
          work as a teacher at l’ésad d’Amiens and for which this course remains an important pedagogical resource. It has therefore been written primarily for my students, to help them understand the key concepts underlying the art of programming and to
          engage them with its creative possibilities.
        </p>
      </div>

      <div class="gridText1">
        <p>
          <i>Designing Programs</i> takes inspiration from <a href="http://www.typeroom.eu/article/memoriam-karl-gerstner-1930-2016" target="_blank">Karl Gerstner’s</a> seminal theoretical book of the same title written in 1964. Gerstner’s work lies
          at the heart of my thinking about a systemic approach to developing artistic ideas - <i>the imaginative use of a rational process.</i>* This procedural approach has a rich history in art and is intrinsically linked with the introduction of
          computers for creation and programming as a mode of thinking. I have strived to make links with that history and indeed with the underlying thinking that is an essential part for students who wish to fully understand the potential of the medium.
        </p>
        <div class="sidenote">
          * Richard Hollis.<br> <a href="http://www.eyemagazine.com/review/article/the-designer-as-programmer" target="blank">
            <div id="sidenoteLink">The Designer as Programmer</div>
          </a>
        </div>
      </div>

      <div class="gridText1">
        <p>
          My main reason for using the web to share this work is quite simply due to its dynamic nature. The printed book, whilst a most perfect medium for reading, is quite a cumbersome form to update and quite incapable of introducing media beyond
          the printed page. Finally, there is only so much one can achieve in the run of an academic year. It is hoped that this computational edition of my course can expand on my teachings. Ultimately, this work aspires not only to acquire the
          fundamental concepts, I want it to encourage my students to dig deeper - that you begin to understand the finer mechanisms of what we call algorithmic thinking.
        </p>
      </div>

      <div class="gridText1">
        <p>
          It should be noted that I use two main programming environments and languages as a means to introduce the concepts in this course:<a href="https://processing.org/tutorials/gettingstarted/" target="_blank"> Processing is a java based
            language </a>developed for artists and designers who want to develop specifically desktop applications. <a href="https://p5js.org/" target="_blank">It’s sister project, P5js</a> follows in a similar tradition of syntax yet is based on
          JavaScript and hence more adapted towards web applications. For a detailed introduction to both these technologies, I advise consulting their respective websites.
        </p>
      </div>

      </br>
      <h2 style="background: black; max-width: 760px; text-align: right;"><a href="01_fundamentals.html">next chapter...</a></h2>


      <!-- Biblio -------------------------->
      </br>
      <h2>Notes</h2>


      <div class="notes">
        Gerstner Karl. <a href="https://www.lars-mueller-publishers.com/designing-programmes" target="_blank">Designing Programmes</a></br>
        Lars Müller Publishers, 2019, originally published 1964.</br></br>

        Fry Ben, Reas Casey. <a href="https://mitpress.mit.edu/books/processing-second-edition" target="_blank">Processing. A Programming Handbook for Visual <br>Designers and Artists</a></br>
        MIT Press, 2014.</br></br>

        Groß Benedikt, et al.<a href="http://www.generative-gestaltung.de/2/" target="_blank">Generative Design</a></br> Princeton Architectural Press, 2019.<br><br>

        Géridan Jean-Michel, Lafargue Jean-Noël. <a href="https://openlibrary.org/books/OL25617851M/Processing" target="_blank">Processing. </br>Le code informatique comme outil de création.</a></br>
        Pearsons, 2011.</br></br>

        Reas Casey, McWilliams Chandler, LUST. <a href="http://formandcode.com/" target="_blank">Form &amp; Code</a></br>
        Princeton Architectural Press. 2012</br></br>

        Ultralab et al. <a href="https://vimeo.com/60735314" target="_blank">Hello World! Processing</a></br>
        Ultralab. 2013</br></br>

      </div>
    </section>
    </br>


    <!-- The Footer -------------------------->
    <section class="body_center">
      <section class="footer">

        <h4>
          ©2019 copyright mark webster
        </h4>
        <p><a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="blank">BitBucket</a> |
          <a href="https://twitter.com/motiondesign_01" target="blank">Twitter</a> |
          <a href="mailto:mark.webster@wanadoo.fr?Subject=Hello%20again" target="_top">Contact</a>
        </p>
      </section>
    </section>

  </section>

</body>

</html>

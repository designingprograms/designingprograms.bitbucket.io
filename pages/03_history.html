<!DOCTYPE html>
<!--
/**

  /\  __-./\  ___\/\  ___\/\ \/\  ___\/\ "-.\ \/\ \/\ "-.\ \/\  ___\
  \ \ \/\ \ \  __\\ \___  \ \ \ \ \__ \ \ \-.  \ \ \ \ \-.  \ \ \__ \
   \ \____-\ \_____\/\_____\ \_\ \_____\ \_\\"\_\ \_\ \_\\"\_\ \_____\
    \/____/ \/_____/\/_____/\/_/\/_____/\/_/ \/_/\/_/\/_/ \/_/\/_____/

   ______ ______  ______  ______  ______  ______  __    __  ______
  /\  == /\  == \/\  __ \/\  ___\/\  == \/\  __ \/\ "-./  \/\  ___\
  \ \  _-\ \  __<\ \ \/\ \ \ \__ \ \  __<\ \  __ \ \ \-./\ \ \___  \
   \ \_\  \ \_\ \_\ \_____\ \_____\ \_\ \_\ \_\ \_\ \_\ \ \_\/\_____\

*
* Web site: https://designingprograms.bitbucket.io
*
* A Medium
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/ END -->
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="DP: Programmed Aesthetics">
  <meta name="keywords" content="Computer art, algorithmic art, algorithmic thinking, graphic, design, art, Computational, Computer art, Aesthetics, art history, New Tendencies, Frieder Nake, Manfred Mohr, Vera Molnar, Max Bense">
  <meta name="author" content="Mark Webster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DP: Preface</title>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145817176-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145817176-2');
</script>


  <!--   Style  -->
  <link rel="stylesheet" href="../css/main.css">

  <!--   Scripts  -->
  <script language="javascript" type="application/javascript" src="../css/nav.js" /></script>

</head>

<body>
  <!-- burgerbars -->
  <div class="container" onclick="myFunction(this)">
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
  </div>

  <!-- sidenav -->
  <div id="mySidenav" class="sidenav">
    <a style="font-family:'IBMPlexMonoBold';font-size: 1.75em;color:black; "href="../index.html">HOME</a><br>
    <a href="00_preface.html">Preface</a>
    <a href="01_fundamentals.html">A Model</a>
    <a href="02_medium.html">A Medium</a>
    <a href="03_history.html">A History</a>
    <a href="04_variables.html">Variables</a>
    <a href="05_functions.html">Functions</a>
    <a href="06_control.html">Control</a>
    <a href="07_loops.html">Loops</a>
    <a href="08_class.html">Classes</a></br>

    <a style="font-family:'IBMPlexMonoLight';color:white; "href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="_blank">Source</a>

    <a style="font-family:'IBMPlexMonoLight';color:black; "href="../code/variable_01.html" target="_blank">Overview</a>
    </div>
    <!-- sidenav FIN -->

  <section id="main">
    <section class="body_center">

   <h1> Programming</br>The Beautiful </h1> <section> <div class="citation_article"> <span
   style="background:blue; color:white;"> “The idea becomes the machine that makes the art.” </span> </div> <p class="legend">Sol LeWitt. Paragraphs on Conceptual Art, 1967</p> </section></br></br>

    <h2>A Very Brief Introduction</h2>
      <div class="gridText1">
      <p>
        Programming has a rich history in the art world, firmly rooted in the early pioneering works of the 1960s and quickly evolving through the algorithmic boom of the 70’s where the concept of the <i>artist programmer</i> begins to take rise. In those early days though, the artist was a marginal user. <i>Computer art</i> as it generically became named, developed mostly in the hands of a lucky few engineers and mathematicians who had access to this rare technology of the time. That, however, was soon to change.
      </p>
    </div>


    <div class="gridText1">
      <p>
      Back then, computers had no interface, programming was the norm and plotters were the staple for visualising the images made. It is beyond the scope of this current text to indulge and give any true depth to how programming became a skill during this relatively short period in our art history. I would however like to point out some key figures, events and movements that are essential for students to know about and which are good entry points for learning more.
      </p>
      <div class="sidenote">
        Back then...
        <a href="https://www.youtube.com/watch?v=TV1iol35fHg" target="_blank">
          <div id="sidenoteLink">
            Computer Generated Art by Frieder Nake.
          </div>
        </a>

      </div>
    </div>

    <div class="gridText1">
      <p>
        Back in 2013, I had the honour of organising along with Julien Gachadoat, a lecture and workshop with one of these pioneering artists from 1960s, the most humble yet wonderfully energetic <a href="https://en.wikipedia.org/wiki/Frieder_Nake" target="_blank">Frieder Nake</a>. Frieder delivered a brilliant lecture that traces his experience of the arrival of the computer in the art world, recounting a passionate story of the first ever exhibition to present algorithmic art work. Entitled <i>Computer-Grafik</i>, the exhibition took place in February 1965 in Stuttgart and presented work by Georg Nees. You can listen to Frieder’s lecture and read more about this event in the notes below.
      </p>
    </div>

    <div class="gridText1">
      <p>
        After that rather modest yet defining moment in art history, other exhibitions ensued. Notably <a href="https://ethw.org/First-Hand:Howard_Wise_Gallery_Show_of_Digital_Art_and_Patterns_(1965):_A_50th_Anniversary_Memoir" target="_blank">a show at the Howard Wise Gallery</a> in New York featuring A. Michael Noll and Béla Julesz. However, it wasn’t until 1968 that two major events were to really make a mark and introduce to the general public this new and emerging medium of the computer in art; <i>Cybernetic Serendipity & Tendencies 4, Computers & Visual Research.</i>
        </p>
    </div>

    <div class="gridText1">
      <p>
         <i>Cybernetic Serendipity</i> took place at the ICA in London 1968 and brought together a variety of artists and scientists working in many fields from installation art to video, music composition, robotics and computer generated art. It was a seminal event that challenged many long-held attitudes towards visual art, especially within the establishment. <i>Tendencies 4</i> took place in Zagreb, Croatia in 1969. Being part of a longer movement -  <i>The New Tendencies (1961-73)</i> - which organised European wide collectives of seminars, exhibitions and research, it had an undeniable influence on the shape of things. Indeed, these two events helped catalyse an interest for the computer yet perhaps more profoundly they also mark a shift in thinking for the artist. The masterpiece and its genius were on the decline and artists were working more and more within a framework and logic of the program, moving away from the object and more towards the process.
        </p>
        <div class="sidenote">
          Cybernetic serendipity
          <a href="https://monoskop.org/Cybernetic_Serendipity" target="_blank">
          <div id="sidenoteLink">
            An online resource.
          </div></br>
          </a>
          New Tendencies
          <a href="https://monoskop.org/New_Tendencies" target="_blank">
            <div id="sidenoteLink">
              An online resource.
            </div>
          </a>
        </div>
    </div>
    <div class="gridText1">
      <p>
        The German philosopher <a href="https://monoskop.org/log/?s=Max+Bense" target="_blank">Max Bense</a> was an influential figure throughout these formative years, instigating in part  <i>Cybernetic Serendipity</i>, along with Jasia Reichardt as curator but perhaps more importantly helping to spread word of this new approach in art. Bense was an active personality and published a number of theoretical works on generative aesthetics building on work in information theory and semiotics. In the early seventies the artist <a href="http://www.emohr.com/" target="_blank">Manfred Mohr</a> was one of the first to take on Bense’s term of the generative. A term that explicitly expressed the medium’s approach; the conception of some system that enables a multitude of various outputs. Mohr endeavoured on a serious artistic career, exploring the realms of pseudo randomness before embracing the cube in all its dimensions. His first major solo exhibition,<a href="http://www.emohr.com/paris-1971/index.html" target="_blank"> <i>Une Esthétique programmée</i> </a> which took place in Paris in 1971 remains a cornerstone exhibition. One in which he clearly sought to instigate the idea of the artist-programmer for the first time.
        </p>
    </div>

    <div class="gridText1">
      <p>
        The Hungarian artist <a href="http://www.veramolnar.com/" target="_blank">Vera Molnar</a> is another major figure of note whose early works which she called <i>“la machine imaginaire”</i> are a wonderful example of her inventing programs in a serial manner and  executing them by hand as if she was the machine. She later went on to explore these early procedural visual grammars with the computer. Her work is a wonderful example of an artist who typically understood algorithmic thinking as an integral part of the creative process and made use of this both with and without the computer.
        </p>
    </div>

    <div class="gridText1">
      <p>
        Theoretical and aesthetical elements of the program and algorithmic thinking as an approach could equally be traced back to the early movements of <i>Constructivism</i> and <i>De Stijl</i>, later evolving into <i>op-art, geometric abstraction and seriality</i>. All these different fields relied on the formal structuring of abstract shape and the stripping down of elements to a bare functional visual plane. The principal approach, or strategy if you like, of structuring these elements relied increasingly on the execution of a pre-organised set of rules. Sol LeWitt’s serial work perhaps exemplifies this neatly and indeed critics were quick to see similarities in his approach and the algorithmic revolution that was taking shape.
        </p>
        <div class="sidenote">
          Frieder Nake<br> <a href="../media/articles/paragraphs_nake.pdf" target="blank">
            <div id="sidenoteLink">Paragraphs On Computer Art, Past & Present</div>
          </a>
        </div>
    </div>

    <div class="gridText1">
      <p>
        Today, those visual abstract elements have become data whilst the rules have become the algorithm and the computer has become the medium. The art of programming is undeniably present, the legacy of the early thinkers and pioneers having laid the foundations for contemporary work that is flourishing. There has been a steady increase in interest over the past decade in the history of computer art and programming. There remains so much more to uncover and explore and I hope that we'll see continued work and research on the subject. I think it is always important to have some historical context to what one is doing, especially in the field of technology where change is so rapid. It invariably opens up windows of interest and is a constant source of inspiration and motivation. I've cited a number of works below that I hope will encourage one to dig deeper. For a first time read, Grant D. Taylor's recent publication is an excellent introduction to the subject with a wealth of references.
        </p>
    </div>
  </br>
      <h2 style="background: black; max-width: 760px; text-align: right;" ><a href="04_variables.html">Concept #01: Variables...</a></h2>


    <!-- Biblio -------------------------->
    </br>
    <h2>Notes</h2>


      <div class="notes">
        British Pathé. <a href="https://www.youtube.com/watch?v=UXvz55V54DI">This is Tomorrow Exhibition Whitechapel Gallery, London 1956</a></br>
        Video published by British Pathé.</br></br>

        Brown Paul et al. <a href="https://mitpress.mit.edu/books/white-heat-cold-logic" target="_blank">White Heat Cold Logic. British Computer Art 1960-1980</a></br>
        MIT Press, 2009.</br></br>

        CPNAS. <a href="https://www.youtube.com/watch?v=5mH9DysESqo">Jasia Reichardt: Cybernetic Serendipity, A Brief Tour 1968</a></br>
        Video published by www.cpnas.org</br></br>

        Couchot Edmond, Hillaire Norbert. <a href="https://www.bloomsbury.com/us/when-the-machine-made-art-9781623562724/" target="_blank">L'art numérique</a></br>
        Flammarion, 2009.</br></br>

        Monoskop. <a href="https://monoskop.org/Computer_art"target="blank">Resources to artists and publications on computer art.</a></br></br>

        Migayrou Fréderic et al. <a href="http://editions-hyx.com/fr/coder-le-monde" target="_blank">Coder le monde</a></br>
        Éditions HYX, 2018.</br></br>

        Nake Frieder, lecture notes. <a href="../media/sound/lecture_nake_part_01.mp3"target="blank"> Audio_01</a> |
        <a href="../media/sound/lecture_nake_part_02.mp3"target="blank"> Audio_02</a> |
        <a href="../media/sound/lecture_nake_part_03.mp3"target="blank"> Audio_03</a> |
        <a href="../media/sound/lecture_nake_part_04.mp3"target="blank"> Audio_04</a></br>
        Musée d'art contemporain de Bordeaux, 2013</br></br>

        Reichardt Jasia. <a href="http://www.studiointernational.com/index.php/cybernetic-serendipity-getting-rid-of-preconceptions-jasia-reichardt">'Cybernetic Serendipity'— Getting rid of preconceptions</a></br>
        Studio International, Vol 176, No 905, November 1968, pp 176-77</br></br>

        Rosen Marget. <a href="https://mitpress.mit.edu/books/little-known-story-about-movement-magazine-and-computers-arrival-art" target="_blank">A Little-Known Story about a Movement,
        a Magazine, and the Computer's Arrival in Art
        New Tendencies and Bit International, 1961–1973</a></br>
        MIT Press, 2011.</br></br>

        Taylor Grant D. <a href="https://www.bloomsbury.com/us/when-the-machine-made-art-9781623562724/" target="_blank">When The Machine Made Art: The Troubled History Of Computer Art</a></br>
        Bloomsbury, 2014.</br></br>

        DAM et al. <a href="http://dam.org/archive/essays/" target="_blank">A selection of essays on digital fine art.</a></br>
        DAM, various years.</br></br>

      </div>
    </section>
</br>

  <!-- The Footer -------------------------->
  <section class="body_center">
    <section class="footer">

      <h4>
        ©2019 copyright mark webster
      </h4>
      <p><a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="blank">BitBucket</a> |
        <a href="https://twitter.com/motiondesign_01" target="blank">Twitter</a> |
        <a href="mailto:mark.webster@wanadoo.fr?Subject=Hello%20again" target="_top">Contact</a>
      </p>
    </section>
  </section>

  </section>
</body>

</html>

<!DOCTYPE html>
<!--
/**

  /\  __-./\  ___\/\  ___\/\ \/\  ___\/\ "-.\ \/\ \/\ "-.\ \/\  ___\
  \ \ \/\ \ \  __\\ \___  \ \ \ \ \__ \ \ \-.  \ \ \ \ \-.  \ \ \__ \
   \ \____-\ \_____\/\_____\ \_\ \_____\ \_\\"\_\ \_\ \_\\"\_\ \_____\
    \/____/ \/_____/\/_____/\/_/\/_____/\/_/ \/_/\/_/\/_/ \/_/\/_____/

   ______ ______  ______  ______  ______  ______  __    __  ______
  /\  == /\  == \/\  __ \/\  ___\/\  == \/\  __ \/\ "-./  \/\  ___\
  \ \  _-\ \  __<\ \ \/\ \ \ \__ \ \  __<\ \  __ \ \ \-./\ \ \___  \
   \ \_\  \ \_\ \_\ \_____\ \_____\ \_\ \_\ \_\ \_\ \_\ \ \_\/\_____\

*
* Web site: https://designingprograms.bitbucket.io
*
* A Medium
* version: V0.05
* Author: m.Webster 2019
* https://area03.bitbucket.io/
*
*/ END -->
<html>

<head>
  <meta charset="UTF-8">
  <meta name="description" content="Designing Programs">
  <meta name="keywords" content="randomness, chance, Design, learning, algorithmic, graphic, Lessons, computational, notebook, control, random, choice, fundamentals">
  <meta name="author" content="Mark Webster">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DP: Control</title>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145817176-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145817176-2');
</script>


  <!--   Style  -->
  <link rel="stylesheet" href="../css/main.css">
  <script language="javascript" type="application/javascript" src="../css/nav.js" />
  </script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.8.0/p5.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.8.0/addons/p5.dom.js"></script>
  <script language="javascript" type="application/javascript" src="../js/control.js" />
  </script>

</head>

<body>
  <!-- burgerbars -->
  <div class="container" onclick="myFunction(this)">
    <div class="bar1"></div>
    <div class="bar2"></div>
    <div class="bar3"></div>
  </div>

  <!-- sidenav -->
  <div id="mySidenav" class="sidenav">
    <a style="font-family:'IBMPlexMonoBold';font-size: 1.75em;color:black; " href="../index.html">HOME</a><br>
    <a href="00_preface.html">Preface</a>
    <a href="01_fundamentals.html">A Model</a>
    <a href="02_medium.html">A Medium</a>
    <a href="03_history.html">A History</a>
    <a href="04_variables.html">Variables</a>
    <a href="05_functions.html">Functions</a>
    <a href="06_control.html">Control</a>
    <a href="07_loops.html">Loops</a>
    <a href="08_class.html">Classes</a></br>

    <a style="font-family:'IBMPlexMonoLight';color:white; "href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="_blank">Source</a>

    <a style="font-family:'IBMPlexMonoLight';color:black; "href="../code/variable_01.html" target="_blank">Overview</a>
    </div>
  <!-- sidenav FIN -->

  <section id="main">
    <section class="body_center">

      <h1>Control</h1>
      <div class="citation_article">
        <span style="background:DarkMagenta; color:white;">
          “Randomness is often used to “humanize” or introduce variation and imperfections to an underlying rigid, deterministic process…”
        </span>
      </div>

      <p class="legend"><a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/raw/1ce25543ff9a13f4ee0ae1e56a274f580a3124ae/media/articles/LEON_a_00533-McCormack.web_.pdf" target ="_blank">Ten Questions Concerning Generative Computer Art. McCormack, Jon et al.</a></p> </br>

      <!-- text center -------------------------->
      <h2>The Structured Programming Paradigm</h2>

      <div class="gridText1">
        <p>
          Most modern day computer languages share a limited number of common structures which can be broken down into three main common parts : <i>sequencing, selection and iteration.</i> These three parts make up what is called the <a href="https://en.wikipedia.org/wiki/Structured_programming" target="_blank"><i>Structured Programming
          Theorem</i></a>, stating that the combination of these three structures is sufficient for expressing any computable process within a program. Historically, this theorem was developed in the 1960s with languages such as Algol and Pascal,
          replacing the need for the famous GOTO statement which, <a href="https://en.wikipedia.org/wiki/Edsger_W._Dijkstra">for some computer scientists,</a> was a cumbersome manner for controlling the flow of a program.
        </p>
      </div>

      <div class="gridText1">
        <p>
          Learning the relationship between these structures and the procedures involved to write them is vital if you want to fully understand the behaviour of the whole process you are attempting to control in a programming environment. Over time,
          it also becomes increasingly important to master the various idioms of these structures. They are the building blocks from which you’ll go on to develop more complex programs. So, having a firm understanding from the outset will help you
          combine these structures in a more efficient and intuitive manner. We are therefore going to spend a little time talking about these in the present and following chapters, especially selection and iteration. To begin with, here’s a short
          introduction to each.
        </p>
      </div>

      <h2>Sequence</h2>
      <div class="gridText1">
        <p>
          Most programs are read and executed in a linear manner. So, code written at the top will be read first, line by line. A sequenced structure is any number of ordered lines of code that are executed in a linear, one way, top to bottom
          manner.
        </p>
      </div>

      <h2>Selection</h2>
      <div class="gridText1">
        <p>
          Selection structures are a powerful means for changing the above behaviour. For example, there are times in a program when you want to jump to another structure but only on a certain condition. To do this, we need a selection structure,
          for example an <i>if statement</i> that says; <i>if this variable equals a certain value then do this.</i>
        </p>
      </div>

      <h2>Iteration</h2>
      <div class="gridText1">
        <p>
          Iteration is similar to selection because it breaks the linearity of a program. However, the particularity of this structure is that it really is dedicated to the repetition of instructions. We'll be taking a more detailed look at this structure in the next chapter.
        </p>
      </div>

      <div class="gridText1">
        <p>Let’s have a look at a simple selection control structure using an <i>if statement.</i></p>
      </div>


      <div class="gridText1">
        <span class="syntax">SYNTAX</span>
      </div>
      <div class="gridText1">
        <code>
          <span class="red"> keyword:if </span>&nbsp;
          <span class="green">&lsaquo;condition&rsaquo;</span>
          <span class="black"> &lsaquo;statement(s);&rsaquo;</span></br></br></br>

          Eg 1. js & Java</br>
          <span class="redCode">if</span>
          <span class="greenCode">(x > 680)</span> <span class="blackCode">{</br>
              &nbsp; x = 0;</br>
              &nbsp; dia = 0;</br>
              }</span></br></br>
        </code>
      </div>

      <div class="gridText1">
        <p>
          To demonstrate our first <i>if statement</i>, we are simply taking the first sketch we wrote for variables and updating the value for x and dia. If x is greater than 680 (width size for our canvas), then we assign both zero to x and dia. The <i>if statement</i> is based on a condition that can be expressed using various relational and/or boolean logical operators.* We can also combine these <i>if statements</i> to define more complex behaviours.
        </p>
        <div class="sidenote">
          * Operators.<br><a href="https://www.tutorialspoint.com/java/java_basic_operators.htm">The Basic Operators</a>
        </div>
      </div>

      <!-- canvas 1 ----------->
      <div id="sketch1"></div> <br>

      <div class="gridText1">
        <p>
          Let's develop a little on this basic control structure and do something quite different. Again, the idea of such a structure is to break the linearity of program flow and this is something we will want to do all the time, especially when developing interactive pieces. A simple yet common use of the <i>if statement</i> can be seen in progams that use keyboard input. Hitting keys may get you letters but we can also tell the machine to do much more when hitting a key. In this next example, we create an interaction that enables us to assign a different fill colour for our shape depending on the key we press.
        </p>
      </div>
      <div class="gridText1">
        <code>
          Eg 2. js & Java</br>
          <span class="redCode">if</span>
          <span class="greenCode">(key == 'a')</span> <span class="blackCode">{</br>
              &nbsp; myColour = color(0, 0, 255);</br>
              }</span></br></br>
        </code>
      </div>

      <div class="gridText1">
        <p>
          In the above snippet of code, the condition tests to see whether we have hit the 'a' key and if so, then we attribute the colour blue to our variable, <i>myColour.</i>
        </p>
        <div class="sidenote">
          Code Source </br>
          <a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/examples_source_code/dp_p5js/C_CONTROL/control_01.js" target="_blank">Sketch: Control_01</a>
        </div>
      </div>

      <!-- canvas 2----------->
      <div id="sketch2"></div></br>

      <h2>Take A Random Walk</h2>
      <div class="gridText1">
        <p>
          The if statement is a vital part of programming when it comes to coding decisions. There exists another important concept that enables us to create descisions that is worthy of mention in this chapter; randomness. Randomness is a vast subject in itself with a wealth of references and books on the matter spanning from the early throwing of dice, the search for predicting outcomes in gambling to the probability theories of Blaise Pascal & Pierre de Fermat, right up to the modern day implementation of Monte Carlo algorithms in machine learning. Its arrival in art is equally well documented, from Mozart's dice compositions to the pioneering algorithmic works of  Manfred Mohr.
        </p>
      </div>

      <div class="gridText1">
        <p>
          Indeed, the use of chance as an integral part of the artistic process became an essential part of early 20th century movements such as Dada, Surrealism and Abstract Expressionism. Notable artists were William Burroughs, Marcel Duchamp, Tristan Tzara, John Cage, to mention but a few. All shared a similar motivation for implementing chance operations as a means for shifting focus away from personal expression and liberating the creative process from the chains of control.
        </p>
      </div>

      <div class="gridText1">
        <p class="citation">"Would it not be possible to describe, with linear purity of a mathematical program, “fields of events” in which random processes could occur? Thus we would have a unique dialectic between chance and program, between mathematics and hazard, between planned concepts and free acceptance of what will happen according to precise, prearranged formative patterns, which do not negate spontaneity, but give it boundaries and potential directions.."
        </p>
        <div class="sidenote">
          Arte Programmata
          <a href="http://www.reprogrammed-art.cc/library/33/Arte-programmata.-Arte-cinetica.-Opere-moltiplicate.-Opera-aperta.">Article</a>

        </div>
      </div>
      <div class="legend">
        Umberto Eco. <a href="http://www.julioleparc.org/umberto-eco.html" target="_blank">Arte Programmata</a>
      </div>

      <div class="gridText1">
        <p>
          Within the field of computer programming, randomness comes packaged as a function that generates pseudorandom values. It is a powerful strategy for breaking the strict uniformity of progammed work. Instead of running a program and always getting the same result, incorporating a little randomness into the process helps in creating variety and indeed surprise. There are many reasons why we may want to introduce some chance into our programs. In more complicated applications, real-world simulations rely a lot on generating unknown variables. From modelling weather systems and physical forces to generating new game play strategies, characters and game terrains. Randomness is a vital part of control structures.
        </p>
      </div>

      <div class="gridText1">
        <p>
          Pretty much all computer languages have some built-in function for generating random values. It is a simple task of calling that function and passing in some values. Let’s have a look at this function with a basic example.
        </p>
      </div>

      <div class="gridText1">
        <span class="syntax">SYNTAX</span>
      </div>
      <div class="gridText1">
        <code>
          <span class="red"> random</span>&nbsp;
          <span class="green">&lsaquo;val min, val max&rsaquo;</span></br></br></br>

          Eg 1. js & Java</br>
          <span class="redCode">random</span>
          <span class="greenCode">(100, 320)</span></br></br>
        </code>
        <div class="sidenote">
          Code Source </br>
          <a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/examples_source_code/dp_p5js/C_CONTROL/control_02.js" target="_blank">Sketch: Control_02</a>
        </div>
      </div>

      <!-- canvas 2----------->
      <div id="sketch3"></div></br>

      <div class="gridText1">
        <span class="observe"><a href="https://observablehq.com/@motiondesign_01/control" target="_blank"> Observe: explore this concept further >>></a></span>
      </div>

      <div class="gridText1">
        <p>
          The way this function works is that we pass in a minimum and maximum value and it will return a sequence of random values within that range. Everytime we call the function, we get a new iteration of the random sequence. This is however only the beginning. The random function may be fine for small scale and basic programs but you should dig a little more deeper into the subject if you want to get better random values. A good starting point is the Coding Math video tutorial cited below. From there, it would be a good idea to explore and learn about the noise function too which is heavily used in simulating more organic form. It's worthy to note that totally random works do not always give the most pleasant aesthetic results formally. Randomness needs some thoughtful tweaking for best results.
        </p>
      </div></br>

      <h2 style="background: black; max-width: 760px; text-align: right;"><a href="07_loops.html">Concept IV: Loops >>></a></h2>

    </section>

    <!-- wide image
  <section class="main_wide">
      <img src="../media/a_variables/naturalForm.png" alt="IMG">
  </section>
-->


    <!-- Biblio -------------------------->
    <section class="body_center">
      </br>
      <h2>Notes</h2>
      <div class="notes">

        Christian, Brian & Griffiths, Tom. <a href="http://algorithmstoliveby.com/" target="_blank">Randomness.(pg182-204)</a></br>
        William Collins, 2017</br></br>

        Montfort, Nick et al. <a href="http://nickm.com/trope_tank/10_PRINT_121114.pdf" target="_blank">Randomness.</a> (pg120-146)</br>
        MIT Press, 2013</br></br>

        Mlodinow, Leonard. <a href="https://www.youtube.com/watch?v=KRLwyq5dK48" target="_blank">The Drunkard's Walk: How Randomness Rules Our Lives.</a> (pg120-146)</br>
        MIT Press, 2013</br></br>

        Haar, Mads. <a href="https://www.random.org/randomness/">Introduction to Randomness and Random Numbers.</a></br>
        Random.org</br></br>

        Stevens, Michael. <a href="https://www.youtube.com/watch?v=9rIy0xY99a0">What is Randomness</a></br>
        YouTube 2014</br></br>

        Coding Math. <a href="https://www.youtube.com/watch?v=4sYawx70iP4">Pseudo Random Number Generators Part I</a></br>
        YouTube 2014</br></br>

      </div>

    </section>
    </br>

    <!-- The Footer -------------------------->
    <section class="body_center">
      <section class="footer">
        <h4>
          ©2019 copyright mark webster
        </h4>
        <p><a href="https://bitbucket.org/designingprograms/designingprograms.bitbucket.io/src/master/" target="blank">BitBucket</a> |
          <a href="https://twitter.com/motiondesign_01" target="blank">Twitter</a> |
          <a href="mailto:mark.webster@wanadoo.fr?Subject=Hello%20again" target="_top">Contact</a>
        </p>
      </section>
    </section>

  </section>
</body>

</html>

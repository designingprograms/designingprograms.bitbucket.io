
  /* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
  var b = isOpen = false;
  function myFunction(x){
      x.classList.toggle("change");
      isOpen = !isOpen;
      if(isOpen){
        openNav();
      }else {
        closeNav();
      }
  }
  /*
  function myFunction2(x){
      closeNav();
      x.classList.toggle("change");
  }
  */
  function openNav() {
      const mq = window.matchMedia( "(min-width: 500px)" );
      if (mq.matches) {
        // window width is at least 500px
        document.getElementById("mySidenav").style.width = "200px";
        document.getElementById("main").style.marginLeft = "200px";
      } else {
        // window width is less than 500px
        document.getElementById("mySidenav").style.width = "75px";
        document.getElementById("main").style.marginLeft = "75px";
      }
      document.body.style.backgroundColor = "rgba(0,0,0)";
      }

      /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
      function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
      document.getElementById("main").style.marginLeft = "0";
      document.body.style.backgroundColor = "white";
}
